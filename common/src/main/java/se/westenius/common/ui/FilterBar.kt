package se.westenius.common.ui

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.TextFieldValue
import se.westenius.common.R

@Composable
fun SearchBar(
    value: MutableState<TextFieldValue>,
    placeHolder: String,
    modifier: Modifier = Modifier,
    trailingIcon: @Composable () -> Unit = {
        Image(
            painter = painterResource(id = R.drawable.ic_outline_filter_list_24),
            colorFilter = ColorFilter.tint(MaterialTheme.colors.onSurface),
            contentDescription = null
        )
    },
    onValueChanged: (String) -> Unit = {}
) {
    var filter by remember { value }

    TextField(
        modifier = modifier.fillMaxWidth().apply { background(Color.Transparent) },
        value = filter,
        onValueChange = { string ->
            filter = string
            onValueChanged(string.text)
        },
        placeholder = {
            Text(text = placeHolder)
        },
        trailingIcon = trailingIcon
    )
}