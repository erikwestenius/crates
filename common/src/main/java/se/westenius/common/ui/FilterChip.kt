package se.westenius.common.ui

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.material.Text
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import se.westenius.theme.CraterTheme

@Composable
fun FilterChip(
    label: String,
    selected: Boolean,
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    shape: Shape = RoundedCornerShape(50)
) {
    Box(
        modifier = modifier
            .clip(shape)
            .clickable(onClick = onClick)
            .background(if (selected) {
                MaterialTheme.colors.primary
            } else {
                Color.Transparent
            }, shape = shape)
            .border(BorderStroke(1.dp, MaterialTheme.colors.primary)),
    ) {
        Text(
            modifier = Modifier
                .padding(horizontal = se.westenius.theme.smallPadding, vertical = se.westenius.theme.smallPadding / 2),
            style = MaterialTheme.typography.body2,
            color = if (selected) {
                MaterialTheme.colors.onPrimary
            } else {
                MaterialTheme.colors.onSurface
            },
            text = label
        )
    }
}

@Preview
@Composable
fun PreviewChip() {
    CraterTheme {
        FilterChip(label = "Artists", selected = false, onClick = {})
    }
}