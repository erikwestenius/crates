package se.westenius.common.ui.components.release

import androidx.compose.runtime.Composable
import se.westenius.common.navigation.Destination
import se.westenius.common.navigation.LocalNavigator
import se.westenius.discogs.collection.repository.data.Release

@Composable
fun ReleaseHolder(release: Release) { //support composable to make shared element transition work
    val navigator = LocalNavigator.current
    if (navigator.current.let {
            !(it is Destination.ReleaseDetail && it.release.id == release.id)
        }) {
        Release(
            release = release,
            onClick = {
                navigator.navigate(Destination.ReleaseDetail(release))
            }
        )
    }
}

@Composable
fun EmphasizedReleaseHolder(release: Release) { //support composable to make shared element transition work
    val navigator = LocalNavigator.current
    if (navigator.current.let {
            !(it is Destination.ReleaseDetail && it.release.id == release.id)
        }) {
        EmphasizedRelease(
            release = release,
            onClick = {
                navigator.navigate(Destination.ReleaseDetail(release))
            }
        )
    }
}