package se.westenius.common.ui

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.*
import se.westenius.common.R
import se.westenius.common.navigation.Actions
import se.westenius.common.navigation.LocalNavigator
import se.westenius.theme.regularPadding
import kotlin.math.abs

val collapsedToolbarHeight = 56.dp
val expandedToolbarHeight = 128.dp

@Composable
fun ScaffoldCollapsingToolbar(
    scrollState: ScrollState = run {
        rememberScrollState(0)
    },
    extraCollapsePadding: Dp = 0.dp,
    collapsedHeight: Dp = collapsedToolbarHeight,
    expandedHeight: Dp = expandedToolbarHeight,
    toolbar: @Composable CollapsingToolbarScope.() -> Unit,
    body: @Composable CollapsingToolbarBodyScope.() -> Unit
) {
    Box {
        val bodyScope = CollapsingToolbarBodyScope(scrollState)
        CollapsingContentScaffolding(
            scrollState = scrollState,
            collapsedToolbarHeight = collapsedHeight,
            expandedToolbarHeight = expandedHeight,
            bodyContent = { bodyScope.body() }
        )

        CollapsingToolbarScaffolding(
            extraCollapsePadding = extraCollapsePadding,
            scrollOffset = scrollState.value.toFloat(),
            collapsedToolbarHeight = collapsedHeight,
            expandedToolbarHeight = expandedHeight,
            collapsingToolbar = toolbar
        )
    }
}

@Composable
fun CollapsingToolbarScaffolding(
    extraCollapsePadding: Dp,
    scrollOffset: Float,
    collapsedToolbarHeight: Dp,
    expandedToolbarHeight: Dp,
    collapsingToolbar: @Composable CollapsingToolbarScope.() -> Unit
) {
    val maxOffset = with(LocalDensity.current) {
        expandedToolbarHeight.toPx() - collapsedToolbarHeight.toPx()
    }

    val offset = with(LocalDensity.current) {
        (extraCollapsePadding.toPx() - scrollOffset).coerceIn(
            minimumValue = -(maxOffset),
            maximumValue = 0F
        )
    }

    val progress = abs(offset / maxOffset).coerceAtMost(1.0F)

    val scope = CollapsingToolbarScope(
        progress = progress
    )

    Column(
        verticalArrangement = Arrangement.Bottom,
        modifier = Modifier
            .fillMaxWidth()
            .height(expandedToolbarHeight - (expandedToolbarHeight - collapsedToolbarHeight) * progress)
    ) {
        Surface(
            elevation = 4.dp.times(progress)
        ) {
            scope.collapsingToolbar()
        }
    }
}

@Composable
fun CollapsingContentScaffolding(
    scrollState: ScrollState,
    collapsedToolbarHeight: Dp,
    expandedToolbarHeight: Dp,
    bodyContent: @Composable (scrollState: ScrollState) -> Unit
) {
    Column {
        Spacer(
            modifier = Modifier.fillMaxWidth()
                .height(collapsedToolbarHeight)
        )
        Column(Modifier.verticalScroll(scrollState)) {
            Spacer(
                modifier = Modifier.fillMaxWidth()
                    .height(expandedToolbarHeight - collapsedToolbarHeight)
            )
            bodyContent(scrollState)
            Spacer(
                modifier = Modifier.fillMaxWidth()
                    .height(expandedToolbarHeight)
            )
        }
    }
}

@Composable
fun CollapsingToolbarScope.CollapsingToolbar(
    title: String,
    showBackButton: Boolean = false
) {
    val navigator = LocalNavigator.current
    val actions = remember(navigator) {
        Actions(navigator)
    }

    Box(
        modifier = Modifier
            .fillMaxHeight()
            .fillMaxWidth()
    ) {
        val translation = if (showBackButton) {
            with(LocalDensity.current) {
                collapsedToolbarHeight.toPx()
            }
        } else {
            0F
        }
        Text(
            modifier = Modifier.align(Alignment.BottomStart)
                .padding(regularPadding)
                .graphicsLayer(translationX = translation * progress),
            text = title,
            style = MaterialTheme.typography.h4,
            fontWeight = FontWeight.Bold,
            fontSize = (40 - progress * 20).sp,
            color = MaterialTheme.colors.onSurface,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis
        )

        if (showBackButton) {
            Box(
                modifier = Modifier
                    .align(alignment = Alignment.BottomStart)
                    .padding(bottom = (expandedToolbarHeight - collapsedToolbarHeight) * (1F - progress))
                    .clip(CircleShape)
            ) {
                BackButton(
                    modifier = Modifier.align(alignment = Alignment.TopStart)
                        .clickable(onClick = {
                            actions.upPress()
                        })
                )
            }
        }
    }
}

@Composable
fun BackButton(
    modifier: Modifier = Modifier,
) {
    Image(
        modifier = modifier
            .size(collapsedToolbarHeight)
            .padding(16.dp),
        colorFilter = ColorFilter.tint(MaterialTheme.colors.onSurface),
        painter = painterResource(id = R.drawable.ic_baseline_arrow_back_24),
        contentDescription = null
    )
}

class CollapsingToolbarScope(
    val progress: Float
)

class CollapsingToolbarBodyScope(
    val scrollState: ScrollState
)