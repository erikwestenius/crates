package se.westenius.common.ui.components.release

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import se.westenius.common.asFormattedPrice
import se.westenius.discogs.collection.repository.data.Release
import se.westenius.theme.regularPadding

@Composable
fun Release(
    release: Release,
    onClick: () -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clickable(
                onClick = { onClick() }
            )
            .padding(
                horizontal = regularPadding,
                vertical = 8.dp
            ),
        verticalAlignment = Alignment.CenterVertically
    ) {
        AlbumArt(
            elevation = 0.dp,
            modifier = Modifier.size(48.dp),
            url = release.thumbnailUrl
        )
        Column(
            modifier = Modifier.padding(horizontal = 16.dp),
            verticalArrangement = Arrangement.SpaceEvenly,
        ) {
            Text(
                text = release.title,
                overflow = TextOverflow.Ellipsis,
                maxLines = 1,
                fontWeight = FontWeight.Bold
            )
            Text(
                text = release.artists.joinToString { it.name },
                overflow = TextOverflow.Ellipsis,
                maxLines = 1
            )
            Text(
                text = release.value.asFormattedPrice(),
                fontWeight = FontWeight.Bold
            )
        }
    }
}