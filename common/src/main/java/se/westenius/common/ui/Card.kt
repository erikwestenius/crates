package se.westenius.common.ui

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Card
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun CraterCard(
    modifier: Modifier = Modifier
        .fillMaxWidth(),
    content: @Composable () -> Unit
) {
    Card(
        modifier = modifier,
        elevation = 2.dp
    ) {
        content()
    }
}