package se.westenius.common.ui

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
fun LoadingState(modifier: Modifier) {
    Box(modifier = modifier.fillMaxWidth()) {
        Center {
            CircularProgressIndicator()
        }
    }
}