package se.westenius.common.ui.crate

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.VectorPainter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import se.westenius.common.ui.CraterCard

@Composable
fun Crate(
    title: @Composable () -> Unit,
    subtitle: String? = null,
    action: @Composable (() -> Unit)? = null,
    content: @Composable () -> Unit
) {
    Column(
        modifier = Modifier
            .padding(horizontal = se.westenius.theme.regularPadding)
    ) {
        CraterCard {
            Column (modifier = Modifier.padding(vertical = se.westenius.theme.regularPadding)) {
                Row(modifier = Modifier.padding(horizontal = se.westenius.theme.regularPadding)) {
                    title()
                }
                subtitle?.let {
                    Row(modifier = Modifier.padding(horizontal = se.westenius.theme.regularPadding)) {
                        Text(text = it)
                    }
                }
                Row(modifier = Modifier.padding(top = se.westenius.theme.smallPadding)) {
                    Column {
                        content()
                    }
                }
                action?.let {
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        it()
                    }
                }
            }
        }
    }
}

@Composable
fun CrateTitle(
    vectorPainter: Painter? = null,
    title: String
) {
    Row(
        horizontalArrangement = Arrangement.spacedBy(16.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        vectorPainter?.let {
            Image(
                painter = it,
                colorFilter = ColorFilter.tint(MaterialTheme.colors.onSurface),
                contentScale = ContentScale.Crop,
                modifier = Modifier.size(32.dp),
                contentDescription = null
            )
        }
        Text(
            text = title,
            style = MaterialTheme.typography.h4
        )
    }
}

@Composable
fun CrateAction(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    text: String
) {
    TextButton(
        modifier = modifier,
        onClick = {
            onClick()
        }
    ) {
        Text(
            text = text,
            fontWeight = FontWeight.Bold,
            color = MaterialTheme.colors.onSurface
        )
    }
}