package se.westenius.common.ui

import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun GenericEmptyState(
    modifier: Modifier = Modifier,
    text: String
) {
    EmptyState {
        Text(
            modifier = modifier.padding(top = 40.dp),
            text = text
        )
    }
}

@Composable
fun EmptyState(
    modifier: Modifier = Modifier,
    content: @Composable () -> Unit
) {
    Center(modifier = modifier) {
        content()
    }
}