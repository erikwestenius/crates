package se.westenius.common.ui

import androidx.compose.material.Text
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import se.westenius.common.R

@Composable
fun ErrorState(message: String?) {
    Center(modifier = Modifier.padding(all = se.westenius.theme.regularPadding)) {
        Text(text = message ?: stringResource(id = R.string.general_error_message))
    }
}