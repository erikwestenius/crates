package se.westenius.common.ui.components.release

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import dev.chrisbanes.accompanist.coil.CoilImage

@Composable
fun AlbumArt(
    modifier: Modifier = Modifier,
    elevation: Dp = 0.dp,
    url: String,
    size: Dp = 48.dp,
    shape: Shape = CircleShape,
    border: BorderStroke? = BorderStroke(0.5.dp, MaterialTheme.colors.onSurface.copy(alpha = 0.15F))
) {
    Surface(
        elevation = elevation,
        shape = shape,
        border = border
    ) {
        CoilImage(
            contentScale = ContentScale.Crop,
            data = url,
            modifier = modifier
                .size(size)
                .clip(shape),
            contentDescription = null
        )
    }
}