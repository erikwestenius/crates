package se.westenius.common.navigation

import android.os.Parcelable
import androidx.compose.runtime.Immutable
import kotlinx.android.parcel.Parcelize
import se.westenius.discogs.collection.repository.data.Release

/**
 * Models the screens in the app and any arguments they require.
 */
sealed class Destination(val isFloating: Boolean = false) : Parcelable {
    @Parcelize
    object Main : Destination()

    @Immutable
    @Parcelize
    data class ReleaseDetail(val release: Release) : Destination(true)

    @Immutable
    @Parcelize
    data class ArtistDetail(val id: Long) : Destination()

    @Immutable
    @Parcelize
    data class LabelDetail(val id: Long) : Destination()

    @Immutable
    @Parcelize
    data class SearchedReleaseDetail(val id: Long) : Destination()

    @Parcelize
    object MostValuableRecordsCrate : Destination()

    @Parcelize
    object ReleasesCurrentlyNFS : Destination()

    @Parcelize
    data class CollectionArtistCrate(val name: String, val releases: List<Release>) : Destination()

    @Parcelize
    data class CollectionLabelCrate(val name: String, val releases: List<Release>) : Destination()

    @Parcelize
    data class CollectionGenreCrate(val name: String, val releases: List<Release>) : Destination()
}

/**
 * Models the navigation actions in the app.
 */
class Actions(navigator: Navigator) {
    val toReleaseDetail: (Release) -> Unit = { release: Release ->
        navigator.navigate(Destination.ReleaseDetail(release))
    }

    val toMostValuableRecordsCrate : () -> Unit = {
        navigator.navigate(Destination.MostValuableRecordsCrate)
    }

    val toReleasesCurrentlyNFS : () -> Unit = {
        navigator.navigate(Destination.ReleasesCurrentlyNFS)
    }

    val toCollectionArtistCrate : (String, List<Release>) -> Unit = { name, releases ->
        navigator.navigate(Destination.CollectionArtistCrate(name, releases))
    }

    val toCollectionLabelCrate : (String, List<Release>) -> Unit = { name, releases ->
        navigator.navigate(Destination.CollectionLabelCrate(name, releases))
    }

    val toCollectionGenreCrate : (String, List<Release>) -> Unit = { name, releases ->
        navigator.navigate(Destination.CollectionGenreCrate(name, releases))
    }

    val toSearchedLabel: (Long) -> Unit = { id: Long ->
        navigator.navigate(Destination.LabelDetail(id))
    }

    val toSearchedArtist: (Long) -> Unit = { id: Long ->
        navigator.navigate(Destination.ArtistDetail(id))
    }

    val toSearchedRelease: (Long) -> Unit = { id: Long ->
        navigator.navigate(Destination.SearchedReleaseDetail(id))
    }

    val upPress: () -> Unit = {
        navigator.back()
    }
}