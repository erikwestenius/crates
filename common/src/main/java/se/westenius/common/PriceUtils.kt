package se.westenius.common

import java.text.NumberFormat
import java.util.*

private val formatter = NumberFormat.getCurrencyInstance()
    .apply {
        maximumFractionDigits = 0
        currency = Currency.getInstance("SEK")
    }

fun Long.asFormattedPrice(): String = if (this == 0L) {
    "Price unknown"
} else {
    formatter.format(this)
}

fun Float.asFormattedPrice(): String = if (this == 0F) {
    "Price unknown"
} else {
    formatter.format(this)
}