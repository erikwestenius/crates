package se.westenius.crates.main

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material.MaterialTheme
import androidx.lifecycle.asLiveData
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import se.westenius.common.util.SystemUiController
import se.westenius.discogs.auth.AuthActivity
import se.westenius.discogs.auth.AuthHandler
import se.westenius.discogs.collection.ContinueUpdateWorker
import se.westenius.discogs.collection.UpdateExtendedCollectionInfoWorker
import se.westenius.record.RecordViewModel
import se.westenius.theme.CraterTheme
import java.time.Duration
import javax.inject.Inject

@FlowPreview
@ExperimentalCoroutinesApi
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var authHandler: AuthHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val systemUiController = SystemUiController(window)
        authHandler.isAuthorized.asLiveData()
            .observe(this) { isAuthorized ->
                if (!isAuthorized) {
                    startActivity(AuthActivity.createIntent(this))
                }
            }
        setContent {
            CraterTheme {
                systemUiController.setSystemBarsColor(MaterialTheme.colors.surface)
                CraterApp(onBackPressedDispatcher)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        scheduleUpdateIfNotFinishedWorker()
    }

    private fun scheduleUpdateIfNotFinishedWorker() {
        val workRequest =
            OneTimeWorkRequestBuilder<UpdateExtendedCollectionInfoWorker>()
                .addTag(ContinueUpdateWorker.TAG)
                .setInitialDelay(Duration.ZERO)
                .build()

        WorkManager.getInstance(this)
            .enqueueUniqueWork(
                ContinueUpdateWorker.TAG,
                ExistingWorkPolicy.KEEP,
                workRequest
            )
    }
}