package se.westenius.crates.main

import androidx.activity.OnBackPressedDispatcher
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.size
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import se.westenius.collection.ui.CollectionScreen
import se.westenius.collection.ui.crates.ReleasesCrate
import se.westenius.common.navigation.Destination
import se.westenius.common.navigation.Navigator
import se.westenius.common.navigation.LocalNavigator
import se.westenius.crates.R
import se.westenius.insights.nfs.NotForSaleScreen
import se.westenius.insights.ui.InsightsScreen
import se.westenius.insights.value.MostValuableRecordsScreen
import se.westenius.record.ui.ReleaseDetailsScreen
import se.westenius.search.ArtistDetailsScreen
import se.westenius.search.LabelDetailsScreen
import se.westenius.search.SearchScreen
import se.westenius.wantlist.ui.WantlistScreen

@ExperimentalCoroutinesApi
@FlowPreview
@Composable
fun CraterApp(backDispatcher: OnBackPressedDispatcher) {

    val selectedTab = remember { mutableStateOf(Tab.Insights) }

    val navigator: Navigator = rememberSaveable(
        saver = Navigator.saver(backDispatcher)
    ) {
        Navigator(Destination.Main, backDispatcher)
    }

    CompositionLocalProvider(LocalNavigator provides navigator) {
        Box {
            when (val destination = navigator.current) {
                is Destination.Main -> {
                    Scaffold(bottomBar = {
                        BottomNavigation(
                            backgroundColor = MaterialTheme.colors.surface
                        ) {
                            Tab.values().forEach { tab ->
                                CraterBottomNavigationItem(tab = tab, selectedTab = selectedTab)
                            }
                        }
                    }) {
                        when (selectedTab.value) {
                            Tab.Insights -> InsightsScreen()
                            Tab.Collection -> CollectionScreen()
                            Tab.Wantlist -> WantlistScreen()
                            Tab.Search -> SearchScreen()
                        }
                    }
                }
                is Destination.MostValuableRecordsCrate -> {
                    MostValuableRecordsScreen()
                }
                is Destination.ReleasesCurrentlyNFS -> {
                    NotForSaleScreen()
                }
                is Destination.SearchedReleaseDetail -> {
                    se.westenius.search.ReleaseDetailsScreen(destination.id)
                }
                is Destination.ArtistDetail -> {
                    ArtistDetailsScreen(destination.id)
                }
                is Destination.LabelDetail -> {
                    LabelDetailsScreen(destination.id)
                }
                is Destination.CollectionLabelCrate -> {
                    ReleasesCrate(destination.name, destination.releases)
                }
                is Destination.CollectionGenreCrate -> {
                    ReleasesCrate(destination.name, destination.releases)
                }
                is Destination.CollectionArtistCrate -> {
                    ReleasesCrate(destination.name, destination.releases)
                }
                is Destination.ReleaseDetail -> {
                    ReleaseDetailsScreen(destination.release)
                }
                else -> {
                    //noop
                }
            }
        }
    }
}

@Composable
fun CraterBottomNavigationItem(tab: Tab, selectedTab: MutableState<Tab>) {
    RowScope.BottomNavigationItem(
        icon = {
            Image(
                modifier = Modifier.size(24.dp),
                painter = painterResource(id = tab.iconRes),
                colorFilter = ColorFilter.tint(
                    MaterialTheme.colors.onSurface.copy
                        (
                        alpha = if (tab == selectedTab.value) {
                            1F
                        } else {
                            0.5F
                        }
                    )
                ),
                contentDescription = null
            )
        },
        selected = tab == selectedTab.value,
        onClick = {
            selectedTab.value = tab
        },
        label = {
            Text(text = stringResource(id = tab.titleRes))
        }
    )
}

enum class Tab(
    @DrawableRes val iconRes: Int,
    @StringRes val titleRes: Int
) {
    Insights(
        R.drawable.ic_outline_insights_24,
        R.string.tab_insights
    ),
    Collection(
        R.drawable.ic_collection,
        R.string.tab_collection
    ),
    Wantlist(
        R.drawable.ic_wantlist,
        R.string.tab_wantlist
    ),
    Search(
        R.drawable.ic_outline_search_24,
        R.string.tab_search
    )
}