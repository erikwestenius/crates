package se.westenius.crates

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import se.westenius.crates.preferences.PreferencesDataStore
import se.westenius.discogs.auth.AuthPreferences
import se.westenius.discogs.network.NetworkPreferences

@ExperimentalCoroutinesApi
@FlowPreview
@Module
@InstallIn(ApplicationComponent::class)
abstract class CraterModule {

    @Binds
    abstract fun bindNetworkPreferences(
        preferences: PreferencesDataStore
    ) : NetworkPreferences

    @Binds
    abstract fun bindAuthPreferences(
        preferences: PreferencesDataStore
    ) : AuthPreferences
}