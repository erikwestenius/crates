package se.westenius.crates.preferences

import android.content.Context
import androidx.datastore.DataStore
import androidx.datastore.preferences.Preferences
import androidx.datastore.preferences.createDataStore
import androidx.datastore.preferences.edit
import androidx.datastore.preferences.preferencesKey
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import se.westenius.discogs.auth.AuthPreferences
import se.westenius.discogs.network.NetworkPreferences
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PreferencesDataStore @Inject constructor(
    @ApplicationContext private val context: Context
): AuthPreferences, NetworkPreferences {

    private val dataStore: DataStore<Preferences> = context.createDataStore(
        "crater_preferences"
    )

    override val userName: Flow<String>
        get() = dataStore.data.map { preferences ->
            preferences[userNameKey] ?: ""
        }

    override suspend fun updateUserName(userName: String) {
        dataStore.edit {preferences ->
            preferences[userNameKey] = userName
        }
    }

    override val authToken: Flow<String>
        get() = dataStore.data.map { preferences ->
            preferences[authTokenKey] ?: ""
        }

    override suspend fun updateAuthToken(token: String) {
        dataStore.edit {preferences ->
            preferences[authTokenKey] = token
        }
    }

    override val authSecret: Flow<String>
        get() = dataStore.data.map { preferences ->
            preferences[authSecretKey] ?: ""
        }

    override suspend fun updateAuthSecret(secret: String) {
        dataStore.edit {preferences ->
            preferences[authSecretKey] = secret
        }
    }

    companion object {
        private val userNameKey = preferencesKey<String>("user_name")
        private val authTokenKey = preferencesKey<String>("oauth_token")
        private val authSecretKey = preferencesKey<String>("oauth_secret")
    }
}