package se.westenius.crates

import android.app.Application
import androidx.hilt.work.HiltWorkerFactory
import androidx.work.*
import dagger.hilt.android.HiltAndroidApp
import se.westenius.discogs.collection.UpdateExtendedCollectionInfoWorker
import timber.log.Timber
import java.time.Duration
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@HiltAndroidApp
class CraterApplication : Application(), Configuration.Provider {

    @Inject
    lateinit var workerFactory: HiltWorkerFactory

    override fun getWorkManagerConfiguration() =
        Configuration.Builder()
            .setWorkerFactory(workerFactory)
            .build()

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())

        setupPeriodicUpdates()
    }

    private fun setupPeriodicUpdates() {
        val myPeriodicWorkRequest =
            PeriodicWorkRequestBuilder<UpdateExtendedCollectionInfoWorker>(
                48,
                TimeUnit.HOURS
            ).addTag(UpdateExtendedCollectionInfoWorker.TAG)
                .setConstraints(
                    Constraints.Builder()
                        .setRequiredNetworkType(NetworkType.CONNECTED)
                        .setRequiresCharging(true)
                        .build()
                )
                .setInitialDelay(Duration.ZERO)
                .build()

        WorkManager.getInstance(this)
            .enqueueUniquePeriodicWork(
                UpdateExtendedCollectionInfoWorker.TAG,
                ExistingPeriodicWorkPolicy.REPLACE,
                myPeriodicWorkRequest
            )
    }
}