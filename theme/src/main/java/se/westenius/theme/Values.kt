package se.westenius.theme

import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

val smallPadding: Dp = 8.dp
val regularPadding: Dp = 16.dp
val largePadding: Dp = 24.dp