package se.westenius.theme

import androidx.compose.ui.graphics.Color

val eerieBlack = Color(0xFF121417)
val charlestonGreen = Color(0xFF212529)
val powderBlue = Color(0xFFB8DBD9)

val ghostWhite = Color(0xFFF8F9FA)
val cultured = Color(0xFFE9ECEF)