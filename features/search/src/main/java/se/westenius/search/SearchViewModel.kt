package se.westenius.search

import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.text.input.TextFieldValue
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*
import se.westenius.discogs.network.Result
import se.westenius.discogs.search.GetSearchResult
import se.westenius.discogs.search.GetSearchResultItem
import se.westenius.discogs.search.SearchRepository
import se.westenius.discogs.search.SearchType
import se.westenius.search.ViewState.*

@FlowPreview
@ExperimentalCoroutinesApi
internal class SearchViewModel @ViewModelInject constructor(
    private val repository: SearchRepository
) : ViewModel() {

    private val _isLoading = MutableStateFlow(false)
    val isLoading: StateFlow<Boolean> = _isLoading

    private val _searchType = MutableStateFlow(SearchType.RELEASE)
    val searchType: StateFlow<SearchType> = _searchType

    val searchTerm = mutableStateOf(TextFieldValue(""))

    val query = MutableStateFlow("")

    val searchResult = query
        .combine(searchType, ::SearchParameter)
        .debounce(DEBOUNCE_TIMEOUT_MILLIS)
        .onEach { _isLoading.value = true }
        .mapLatest(::createSearchViewState)
        .catch { Error(it.message) }
        .onStart { Initial }
        .onEach { _isLoading.value = false }
        .distinctUntilChanged()
        .asLiveData()

    private suspend fun createSearchViewState(parameter: SearchParameter): ViewState {
        return parameter.query.takeIf { it.isNotBlank() }
            ?.let { query -> repository.search(query, parameter.type) }
            ?.toViewState()
            ?: Initial
    }

    private fun Result<GetSearchResult>.toViewState() = when (this) {
        is Result.Success -> when {
            value.results.isEmpty() -> Empty
            else -> Success(value.results)
        }
        is Result.Error -> Error(message)
    }

    fun onTypeClick(type: SearchType) {
        _searchType.value = type
    }

    companion object {
        private const val DEBOUNCE_TIMEOUT_MILLIS: Long = 500
    }

    private data class SearchParameter(
        val query: String,
        val type: SearchType
    )
}


internal sealed class ViewState {
    object Initial : ViewState()
    object Empty : ViewState()
    data class Error(val message: String?) : ViewState()
    data class Success(val list: List<GetSearchResultItem>) : ViewState()
}
