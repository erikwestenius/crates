package se.westenius.search

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*
import se.westenius.discogs.collection.repository.CollectionRepository
import se.westenius.discogs.collection.repository.data.ReleaseInfo

@FlowPreview
@ExperimentalCoroutinesApi
internal class ReleaseDetailsViewModel @ViewModelInject constructor(
    private val collectionRepository: CollectionRepository
) : ViewModel() {

    val releaseId = MutableStateFlow(0L)

    val release = releaseId
        .onStart { ViewState.Loading }
        .map {
            val release = collectionRepository.releaseInfo(it).firstOrNull()
            ViewState.Success(release!!)
        }
        .catch { ViewState.Error(it.message) }

    sealed class ViewState {
        object Initial : ViewState()
        object Loading : ViewState()
        data class Success(
            val releaseInfo: ReleaseInfo
        ) : ViewState()
        data class Error(val message: String?) : ViewState()
    }
}
