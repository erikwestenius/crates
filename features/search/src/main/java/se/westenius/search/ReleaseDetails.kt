package se.westenius.search

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import se.westenius.common.ui.CollapsingToolbar
import se.westenius.common.ui.ErrorState
import se.westenius.common.ui.LoadingState
import se.westenius.common.ui.ScaffoldCollapsingToolbar
import se.westenius.discogs.collection.repository.data.Genre
import se.westenius.discogs.collection.repository.data.ReleaseInfo
import se.westenius.record.ui.*
import se.westenius.search.ReleaseDetailsViewModel.ViewState.*

@FlowPreview
@ExperimentalCoroutinesApi
@Composable
fun ReleaseDetailsScreen(id: Long) {
    val viewModel = viewModel<ReleaseDetailsViewModel>()
    viewModel.releaseId.value = id

    val viewState by viewModel<ReleaseDetailsViewModel>()
        .release
        .collectAsState(Loading)

    ScaffoldCollapsingToolbar(toolbar = {
        CollapsingToolbar(
            title = stringResource(id = R.string.search_releaseDetails_title),
            showBackButton = true
        )
    }) {
        when (val state = viewState) {
            is Success -> ReleaseDetailsContent(state.releaseInfo)
            is Loading -> LoadingState(Modifier.height(400.dp))
            is Error -> ErrorState(message = state.message)
        }
    }
}

@Composable
fun ReleaseDetailsContent(releaseInfo: ReleaseInfo) {
    Column(
        modifier = Modifier.fillMaxSize()
            .padding(se.westenius.theme.regularPadding),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(se.westenius.theme.regularPadding)
    ) {
        releaseInfo.thumbnailUrl?.let {
            RecordAlbumArt(
                url = it
            )
        }
        Text(
            text = releaseInfo.title,
            style = MaterialTheme.typography.h5
        )
        Text(
            text = releaseInfo.artists.joinToString { it.name },
            style = MaterialTheme.typography.h6
        )
        Text(
            text = releaseInfo.year.toString(),
            style = MaterialTheme.typography.h4
        )
    }

    Crossfade(targetState = releaseInfo) {
        Column {
            Genres(genres = it.genres.map { Genre(it) })

            if (releaseInfo.labels.isNotEmpty()) {
                Labels(labels = releaseInfo.labels)
            }

            TrackList(releaseInfo.trackList)

            releaseInfo.notes?.let {
                Notes(notes = it)
            }
        }
    }
}

