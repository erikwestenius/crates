package se.westenius.search

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*
import se.westenius.discogs.network.Result
import se.westenius.discogs.search.GetArtistResult
import se.westenius.discogs.search.SearchRepository
import se.westenius.search.ArtistDetailsViewModel.ViewState.*

@FlowPreview
@ExperimentalCoroutinesApi
internal class ArtistDetailsViewModel @ViewModelInject constructor(
    private val repository: SearchRepository
) : ViewModel() {

    private val _isLoading = MutableStateFlow(false)
    val isLoading: StateFlow<Boolean> = _isLoading

    val artistId = MutableStateFlow(0L)

    val artist = artistId
        .onStart { Initial }
        .mapLatest {
            when (val result = repository.getArtist(it)) {
                is Result.Success -> Success(result.value)
                is Result.Error -> Error(result.message)
            }
        }
        .catch { Error(it.message) }

    sealed class ViewState {
        object Initial : ViewState()
        object Loading : ViewState()
        data class Success(val artist: GetArtistResult) : ViewState()
        data class Error(val message: String?) : ViewState()
    }
}
