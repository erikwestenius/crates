package se.westenius.search

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.remember
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.font.FontWeight.Companion.Bold
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import se.westenius.common.navigation.Actions
import se.westenius.common.navigation.LocalNavigator
import se.westenius.common.ui.*
import se.westenius.discogs.search.GetSearchResultItem
import se.westenius.discogs.search.SearchType
import se.westenius.search.ViewState.*

@FlowPreview
@ExperimentalCoroutinesApi
@Composable
fun SearchScreen() {
    ScaffoldCollapsingToolbar(
        toolbar = {
            CollapsingToolbar(title = "Search")
        },
        body = {
            SearchContent()
        })
}

@FlowPreview
@ExperimentalCoroutinesApi
@Composable
fun SearchContent() {
    SearchInput()
    SearchResultContent()
}

@FlowPreview
@ExperimentalCoroutinesApi
@Composable
fun SearchResultContent() {
    val searchResult by viewModel<SearchViewModel>()
        .searchResult
        .observeAsState(Initial)

    val type by viewModel<SearchViewModel>()
        .searchType
        .collectAsState()

    val navigator = LocalNavigator.current
    val actions = remember(navigator) {
        Actions(navigator)
    }

    Filter()
    Column(
        modifier = Modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.spacedBy(se.westenius.theme.regularPadding)
    ) {
        Crossfade(
            targetState = searchResult
        ) { result ->
            when (result) {
                Initial -> GenericEmptyState(text = stringResource(id = R.string.search_initialStateMessage_label))
                Empty -> GenericEmptyState(text = stringResource(id = R.string.search_emptyStateMessage_label))
                is Error -> ErrorState(result.message)
                is Success -> Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .fillMaxHeight()
                ) {
                    result.list.map {
                        when (type) {
                            SearchType.RELEASE -> SearchResultReleaseItem(it) {
                                actions.toSearchedRelease(
                                    it.id
                                )
                            }
                            SearchType.ARTIST -> SearchResultTitleItem(it) {
                                actions.toSearchedArtist(
                                    it.id
                                )
                            }
                            SearchType.LABEL -> SearchResultTitleItem(it) {
                                actions.toSearchedLabel(
                                    it.id
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}

@FlowPreview
@ExperimentalCoroutinesApi
@Composable
fun SearchInput() {
    val viewModel = viewModel<SearchViewModel>()
    val isLoading by viewModel.isLoading.collectAsState(false)

    Column(
        modifier = Modifier
            .padding(horizontal = se.westenius.theme.regularPadding)
            .padding(bottom = se.westenius.theme.regularPadding)
    ) {
        SearchBar(
            value = viewModel.searchTerm,
            placeHolder = stringResource(id = R.string.search_placeHolder_label),
            trailingIcon = {
                Crossfade(targetState = isLoading) {
                    if (it) {
                        CircularProgressIndicator(
                            modifier = Modifier.size(24.dp)
                        )
                    } else {
                        Image(
                            painter = painterResource(id = R.drawable.ic_outline_filter_list_24),
                            modifier = Modifier.size(24.dp),
                            colorFilter = ColorFilter.tint(MaterialTheme.colors.onSurface),
                            contentDescription = null
                        )
                    }
                }
            }
        ) {
            viewModel.query.value = it
        }
    }
}

@Composable
fun SearchResultReleaseItem(
    result: GetSearchResultItem,
    onClick: () -> (Unit)
) {
    Row(
        Modifier
            .fillMaxWidth()
            .clickable(onClick = onClick)
            .padding(horizontal = se.westenius.theme.regularPadding, vertical = se.westenius.theme.smallPadding)
    ) {
        Column(
            verticalArrangement = Arrangement.spacedBy(se.westenius.theme.smallPadding),
        ) {
            Text(
                fontWeight = Bold,
                style = MaterialTheme.typography.h6,
                overflow = TextOverflow.Ellipsis,
                maxLines = 1,
                text = result.title
            )
            result.label?.let {
                Text(
                    style = MaterialTheme.typography.body1,
                    text = it.take(3).joinToString(),
                    overflow = TextOverflow.Ellipsis,
                    maxLines = 1
                )
            }
            result.year?.let {
                Text(
                    style = MaterialTheme.typography.body1,
                    text = it
                )
            }
            result.style?.let {
                Text(
                    style = MaterialTheme.typography.body1,
                    text = it.joinToString()
                )
            }
        }
    }
}

@Composable
fun SearchResultTitleItem(
    result: GetSearchResultItem,
    onClick: () -> (Unit)
) {
    Row(
        Modifier
            .fillMaxWidth()
            .clickable(onClick = onClick)
            .padding(horizontal = se.westenius.theme.regularPadding, vertical = 8.dp)
    ) {
        Column(
            verticalArrangement = Arrangement.SpaceEvenly,
        ) {
            Text(
                style = TextStyle(fontWeight = FontWeight.Normal),
                fontSize = 16.sp,
                text = result.title
            )
        }
    }
}


@FlowPreview
@ExperimentalCoroutinesApi
@Composable
fun Filter() {
    Column(
        modifier = Modifier.padding(horizontal = se.westenius.theme.regularPadding)
            .fillMaxWidth()
            .padding(bottom = se.westenius.theme.regularPadding),
        verticalArrangement = Arrangement.spacedBy(se.westenius.theme.regularPadding * 2)
    ) {
        FilterCategories()
    }
}

@ExperimentalCoroutinesApi
@FlowPreview
@Composable
fun FilterCategories() {
    val viewModel = viewModel<SearchViewModel>()
    val type by viewModel<SearchViewModel>()
        .searchType
        .collectAsState()

    Row(
        modifier = Modifier.padding(vertical = se.westenius.theme.regularPadding),
        horizontalArrangement = Arrangement.spacedBy(se.westenius.theme.smallPadding)
    ) {
        FilterChip(
            "Release",
            type == SearchType.RELEASE,
            onClick = { viewModel.onTypeClick(SearchType.RELEASE) })
        FilterChip(
            "Artist",
            type == SearchType.ARTIST,
            onClick = { viewModel.onTypeClick(SearchType.ARTIST) })
        FilterChip(
            "Label",
            type == SearchType.LABEL,
            onClick = { viewModel.onTypeClick(SearchType.LABEL) })
    }
}


