package se.westenius.search

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.viewmodel.compose.viewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import se.westenius.common.ui.CollapsingToolbar
import se.westenius.common.ui.ErrorState
import se.westenius.common.ui.ScaffoldCollapsingToolbar
import se.westenius.search.LabelDetailsViewModel.ViewState.*

@FlowPreview
@ExperimentalCoroutinesApi
@Composable
fun LabelDetailsScreen(id: Long) {
    val viewModel = viewModel<LabelDetailsViewModel>()
    viewModel.labelId.value = id

    val viewState by viewModel<LabelDetailsViewModel>()
        .label
        .collectAsState(Initial)

    Surface(
        modifier = Modifier.fillMaxSize(),
        color = MaterialTheme.colors.surface
    ) {
        ScaffoldCollapsingToolbar(toolbar = {
            CollapsingToolbar(
                title = stringResource(id = R.string.search_labelDetails_title),
                showBackButton = true
            )
        }) {
            when (val state = viewState) {
                is Success -> {
                    Column(Modifier.padding(se.westenius.theme.regularPadding)) {
                        Text(text = state.label.profile ?: "-")
                        Text(text = state.label.id.toString())
                        Text(text = state.label.sublabels?.toString() ?: "-")
                    }
                }
                is Error -> ErrorState(message = state.message)
            }
        }
    }
}
