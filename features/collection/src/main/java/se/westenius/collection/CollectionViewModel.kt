package se.westenius.collection

import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.text.input.TextFieldValue
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.map
import dagger.hilt.android.scopes.ActivityScoped
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import se.westenius.discogs.collection.repository.CollectionRepository
import se.westenius.discogs.collection.repository.data.Release
import java.util.*

@ExperimentalCoroutinesApi
@ActivityScoped
class CollectionViewModel @ViewModelInject constructor(
    repository: CollectionRepository
) : ViewModel() {

    val filterState = mutableStateOf(TextFieldValue(""))
    val filter = MutableStateFlow("")

    private val _collectionFilterType = MutableStateFlow(FilterType.Artist)
    val collectionFilterType: StateFlow<FilterType> = _collectionFilterType

    fun setFilterType(collectionType: FilterType) {
        _collectionFilterType.value = collectionType
    }

    val filteredReleases: LiveData<List<Release>> =
        filter.combine(repository.releases) { filter, releases ->
            releases.filter {
                it.matchesFilter(filter)
            }
        }.asLiveData()

    val releasesByArtists = filteredReleases
        .map { releases ->
            releases.flatMap { release ->
                release.artists.map { artist ->
                    Pair(artist, release)
                }
            }.groupBy { (artist, _) ->
                artist.name.toLowerCase(Locale.getDefault())
            }.map { (_, releases) ->
                Pair(releases.first().first, releases.map { it.second })
            }
        }

    val releasesByLabel = filteredReleases
        .map { releases ->
            releases.flatMap { release ->
                release.labels.map { label ->
                    Pair(label, release)
                }
            }.groupBy { (label, _) ->
                label.name.toLowerCase(Locale.getDefault())
            }.map { (_, releases) ->
                Pair(releases.first().first, releases.map { it.second })
            }
        }

    val releasesByGenre = filteredReleases
        .map { releases ->
            releases.flatMap { release ->
                release.genres.map { genre ->
                    Pair(genre, release)
                }
            }.groupBy { (genre, _) ->
                genre.name.toLowerCase(Locale.getDefault())
            }.map { (_, releases) ->
                Pair(releases.first().first, releases.map { it.second })
            }
        }
}

enum class FilterType {
    Artist, Label, Genre, Release
}