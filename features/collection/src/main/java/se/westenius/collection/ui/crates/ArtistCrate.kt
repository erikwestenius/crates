package se.westenius.collection.ui.crates

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.viewmodel.compose.viewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import se.westenius.collection.CollectionViewModel
import se.westenius.collection.R
import se.westenius.common.navigation.Actions
import se.westenius.common.navigation.LocalNavigator
import se.westenius.common.ui.components.release.EmphasizedReleaseHolder
import se.westenius.common.ui.components.release.ReleaseHolder
import se.westenius.common.ui.crate.Crate
import se.westenius.common.ui.crate.CrateAction
import se.westenius.common.ui.crate.CrateTitle
import se.westenius.discogs.collection.repository.data.Artist
import se.westenius.discogs.collection.repository.data.Release

@ExperimentalCoroutinesApi
@Composable
fun ReleasesByArtists() {
    val artists by viewModel<CollectionViewModel>().releasesByArtists
        .observeAsState(initial = emptyList())

    val showingArtists = artists
        .sortedBy { it.first.name }
        .take(25)

    val numberOfArtists = artists.size

    Crossfade(targetState = numberOfArtists) {
        Text(
            modifier = Modifier.padding(horizontal = se.westenius.theme.regularPadding),
            text = stringResource(
                id = R.string.collection_showingNumberOfArtists_format_label,
                showingArtists.size,
                numberOfArtists
            )
        )
    }

    showingArtists
        .forEach { (artist, releases) ->
            ArtistCrate(
                artist = artist,
                releases = releases
            )
        }
}

@Composable
fun ArtistCrate(
    artist: Artist,
    releases: List<Release>
) {

    val navigator = LocalNavigator.current
    val actions = remember(navigator) {
        Actions(navigator)
    }

    Crate(
        title = {
            CrateTitle(title = artist.name)
        },
        subtitle = stringResource(
            id = R.string.collection_numberOfReleases_format_label,
            releases.size
        ),
        action = {
            if (releases.size > 3) {
                CrateAction(onClick = {
                    actions.toCollectionArtistCrate(artist.name, releases)
                }, text = stringResource(id = R.string.crate_digThisCrate_action))
            }
        },
    ) {
        releases.sortedBy { it.title }
            .take(3)
            .forEachIndexed { index, release ->
                if (index == 0) {
                    EmphasizedReleaseHolder(release = release)
                } else {
                    ReleaseHolder(release = release)
                }
            }
    }
}
