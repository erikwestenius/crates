package se.westenius.collection.ui.crates

import androidx.compose.runtime.Composable
import kotlinx.coroutines.ExperimentalCoroutinesApi
import se.westenius.common.ui.CollapsingToolbar
import se.westenius.common.ui.ScaffoldCollapsingToolbar
import se.westenius.common.ui.components.release.ReleaseHolder
import se.westenius.discogs.collection.repository.data.Release

@ExperimentalCoroutinesApi
@Composable
fun ReleasesCrate(name: String, releases: List<Release>) {
    ScaffoldCollapsingToolbar(toolbar = {
        CollapsingToolbar(
            title = name,
            showBackButton = true
        )
    }) {
        ReleasesCrateContent(releases)
    }
}

@Composable
fun ReleasesCrateContent(releases: List<Release>) {
    val showingReleases = releases
        .sortedBy { it.title }

    showingReleases.forEach {
        ReleaseHolder(release = it)
    }
}