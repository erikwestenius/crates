package se.westenius.collection.ui

import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import se.westenius.collection.CollectionViewModel
import se.westenius.collection.FilterType
import se.westenius.collection.R
import se.westenius.common.ui.FilterChip
import se.westenius.common.ui.SearchBar
import se.westenius.theme.CraterTheme

@ExperimentalCoroutinesApi
@Composable
fun FilterSection() {
    val viewModel = viewModel<CollectionViewModel>()

    Column(
        modifier = Modifier.padding(horizontal = se.westenius.theme.regularPadding)
            .fillMaxWidth()
            .padding(bottom = se.westenius.theme.regularPadding),
        verticalArrangement = Arrangement.spacedBy(se.westenius.theme.regularPadding * 2)
    ) {
        SearchBar(
            value = viewModel.filterState,
            placeHolder = stringResource(id = R.string.filter_placeholder_label),
            onValueChanged = {
                viewModel.filter.value = it
            }
        )

        FilterCategories()
    }
}

@ExperimentalCoroutinesApi
@Composable
fun FilterCategories() {
    val viewModel = viewModel<CollectionViewModel>()
    val type by viewModel.collectionFilterType.collectAsState()

    Row(
        modifier = Modifier,
        horizontalArrangement = Arrangement.spacedBy(se.westenius.theme.smallPadding)
    ) {
        FilterChip(
            "Artists",
            type == FilterType.Artist,
            onClick = { viewModel.setFilterType(FilterType.Artist) }
        )
        FilterChip(
            "Labels",
            type == FilterType.Label,
            onClick = { viewModel.setFilterType(FilterType.Label) }
        )
        FilterChip(
            "Genres",
            type == FilterType.Genre,
            onClick = { viewModel.setFilterType(FilterType.Genre) }
        )
        FilterChip(
            "Releases",
            type == FilterType.Release,
            onClick = { viewModel.setFilterType(FilterType.Release) }
        )
    }
}

@ExperimentalCoroutinesApi
@Preview
@Composable
fun previewFilterBar() {
    CraterTheme {
        FilterCategories()
    }
}