package se.westenius.collection.ui.crates

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.viewmodel.compose.viewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import se.westenius.collection.CollectionViewModel
import se.westenius.collection.R
import se.westenius.common.navigation.Actions
import se.westenius.common.navigation.LocalNavigator
import se.westenius.common.ui.components.release.EmphasizedReleaseHolder
import se.westenius.common.ui.components.release.ReleaseHolder
import se.westenius.common.ui.crate.Crate
import se.westenius.common.ui.crate.CrateAction
import se.westenius.common.ui.crate.CrateTitle
import se.westenius.discogs.collection.repository.data.Label
import se.westenius.discogs.collection.repository.data.Release

@ExperimentalCoroutinesApi
@Composable
fun ReleasesByLabel() {
    val labels by viewModel<CollectionViewModel>()
        .releasesByLabel
        .observeAsState(initial = emptyList())

    val showingLabels = labels
        .sortedBy { it.first.name }
        .take(25)

    val numberOfLabels = labels.size

    Crossfade(targetState = numberOfLabels) {
        Text(
            modifier = Modifier.padding(horizontal = se.westenius.theme.regularPadding),
            text = stringResource(
                id = R.string.collection_showingNumberOfLabels_format_label,
                showingLabels.size,
                numberOfLabels
            )
        )
    }

    showingLabels
        .forEach { (label, releases) ->
            LabelCrate(
                label = label,
                releases = releases
            )
        }
}

@Composable
fun LabelCrate(
    label: Label,
    releases: List<Release>
) {
    val navigator = LocalNavigator.current
    val actions = remember(navigator) {
        Actions(navigator)
    }

    Crate(
        title = {
            CrateTitle(title = label.name)
        },
        subtitle = stringResource(
            id = R.string.collection_numberOfReleases_format_label,
            releases.size
        ),
        action = {
            if (releases.size > 3) {
                CrateAction(onClick = {
                    actions.toCollectionLabelCrate(label.name, releases)
                }, text = stringResource(id = R.string.crate_digThisCrate_action))
            }
        },
    ) {
        releases.sortedBy { it.title }
            .take(3)
            .forEachIndexed { index, release ->
                if (index == 0) {
                    EmphasizedReleaseHolder(release = release)
                } else {
                    ReleaseHolder(release = release)
                }
            }
    }
}
