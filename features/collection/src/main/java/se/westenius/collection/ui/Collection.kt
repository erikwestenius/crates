package se.westenius.collection.ui

import androidx.compose.animation.Crossfade
import androidx.compose.material.Text
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.viewmodel.compose.viewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import se.westenius.collection.CollectionViewModel
import se.westenius.collection.FilterType
import se.westenius.collection.R
import se.westenius.collection.ui.crates.ReleasesByArtists
import se.westenius.collection.ui.crates.ReleasesByGenre
import se.westenius.collection.ui.crates.ReleasesByLabel
import se.westenius.common.ui.CollapsingToolbar
import se.westenius.common.ui.ScaffoldCollapsingToolbar
import se.westenius.common.ui.components.release.ReleaseHolder

@ExperimentalCoroutinesApi
@Composable
fun CollectionScreen() {
    ScaffoldCollapsingToolbar(
        toolbar = {
            CollapsingToolbar(
                title = stringResource(id = R.string.tab_collection)
            )
        }
    ) {
        CollectionContent()
    }
}

@ExperimentalCoroutinesApi
@Composable
fun CollectionContent() {
    val filterType by viewModel<CollectionViewModel>()
        .collectionFilterType
        .collectAsState(FilterType.Artist)

    Column(
        modifier = Modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.spacedBy(se.westenius.theme.regularPadding)
    ) {

        FilterSection()
        when (filterType) {
            FilterType.Artist -> {
                ReleasesByArtists()
            }
            FilterType.Label -> {
                ReleasesByLabel()
            }
            FilterType.Genre -> {
                ReleasesByGenre()
            }
            FilterType.Release -> {
                Releases()
            }
        }
    }
}

@ExperimentalCoroutinesApi
@Composable
fun Releases() {
    val viewModel = viewModel<CollectionViewModel>()
    val filteredReleases by viewModel.filteredReleases
        .observeAsState(initial = emptyList())

    val showingReleases = filteredReleases
        .sortedBy { it.title }
        .take(25)

    val numberOfReleases = filteredReleases.size

    Crossfade(targetState = numberOfReleases) {
        Text(
            modifier = Modifier.padding(horizontal = se.westenius.theme.regularPadding),
            text = stringResource(
                id = R.string.collection_showingNumberOfReleases_format_label,
                showingReleases.size,
                numberOfReleases
            )
        )
    }

    showingReleases.forEach {
        ReleaseHolder(release = it)
    }
}