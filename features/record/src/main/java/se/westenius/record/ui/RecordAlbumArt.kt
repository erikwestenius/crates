package se.westenius.record.ui

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import dev.chrisbanes.accompanist.coil.CoilImage

@Composable
fun RecordAlbumArt(
    modifier: Modifier = Modifier,
    elevation: Dp = 0.dp,
    url: String,
    size: Dp = 96.dp,
    shape: Shape = CircleShape,
    border: BorderStroke? = BorderStroke(0.5.dp, MaterialTheme.colors.onSurface.copy(alpha = 0.15F))
) {
    Surface(
        elevation = elevation,
        shape = shape,
        color = se.westenius.theme.charlestonGreen,
        border = border
    ) {
        CoilImage(
            contentScale = ContentScale.Crop,
            data = url,
            modifier = modifier
                .size(size)
                .clip(shape),
            contentDescription = null
        )
    }
}

@Preview
@Composable
fun PreviewAlbumArt() {
    RecordAlbumArt(url ="https://img.discogs.com/2e3s-w-JF4jdIeUTkIXjTsJY9Kw=/fit-in/600x600/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-249504-1334592212.jpeg.jpg" )
}