package se.westenius.record

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import dagger.hilt.android.scopes.FragmentScoped
import se.westenius.discogs.collection.repository.CollectionRepository
import se.westenius.discogs.collection.repository.data.ReleaseInfo

@FragmentScoped
class RecordViewModel @ViewModelInject constructor(
    private val repository: CollectionRepository
) : ViewModel() {
    fun recordWithId(recordId: Long): LiveData<ReleaseInfo> =
        repository.releaseInfo(recordId)
            .asLiveData()
}

