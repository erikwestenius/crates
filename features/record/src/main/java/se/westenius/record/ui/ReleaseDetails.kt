package se.westenius.record.ui

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import se.westenius.common.asFormattedPrice
import se.westenius.common.ui.*
import se.westenius.discogs.collection.repository.data.Genre
import se.westenius.discogs.collection.repository.data.Label
import se.westenius.discogs.collection.repository.data.Release
import se.westenius.discogs.collection.repository.data.Track
import se.westenius.record.R
import se.westenius.record.RecordViewModel
import se.westenius.record.RecordViewModel_AssistedFactory

@Composable
fun ReleaseDetailsScreen(release: Release) {
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = MaterialTheme.colors.surface
    ) {
        ScaffoldCollapsingToolbar(toolbar = {
            CollapsingToolbar(
                title = stringResource(id = R.string.record_recordDetailsTitle_label),
                showBackButton = true
            )
        }) {
            ReleaseDetailsContent(release = release)
        }
    }
}

@Composable
fun ReleaseDetailsContent(release: Release) {

    val releaseInfo by viewModel<RecordViewModel>()
        .recordWithId(release.id) //todo get extended record info instead
        .observeAsState()

    Column(
        modifier = Modifier.fillMaxSize()
            .padding(se.westenius.theme.regularPadding),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(se.westenius.theme.regularPadding)
    ) {
        RecordAlbumArt(
            url = release.thumbnailUrl
        )
        Text(
            text = release.title,
            style = MaterialTheme.typography.h5
        )
        Text(
            text = release.artists.joinToString { it.name },
            style = MaterialTheme.typography.h6
        )
        Text(
            text = release.value.asFormattedPrice(),
            style = MaterialTheme.typography.h4
        )
    }

    Crossfade(targetState = releaseInfo) {
        Column {
            releaseInfo?.let { info ->
                Genres(genres = release.genres)

                if (release.labels.isNotEmpty()) {
                    Labels(labels = release.labels)
                }

                TrackList(info.trackList)

                info.notes?.let {
                    Notes(notes = it)
                }

            } ?: LoadingState(Modifier.height(400.dp))
        }
    }
}

@Composable
fun TrackList(tracks: List<Track>) {
    Column(
        modifier = Modifier.fillMaxWidth()
            .padding(vertical = se.westenius.theme.regularPadding),
        verticalArrangement = Arrangement.spacedBy(se.westenius.theme.regularPadding)
    ) {
        Header(
            modifier = Modifier.padding(horizontal = se.westenius.theme.regularPadding),
            stringResource(id = R.string.releaseDetails_trackListHeader_label)
        )
        tracks.forEach { track ->
            Track(track)
        }
    }
}

@Composable
fun Track(track: Track) {
    Row(
        modifier = Modifier.padding(
            horizontal = se.westenius.theme.regularPadding
        )
    ) {
        Text(
            modifier = Modifier.padding(end = se.westenius.theme.regularPadding),
            text = track.position
        )
        Text(
            modifier = Modifier.padding(end = se.westenius.theme.regularPadding),
            text = track.title
        )
        Text(text = track.duration)
    }
}

@Composable
fun Notes(notes: String) {
    val sanitizedNotes = notes.replace(regex = Regex("\\[(.*?)\\]"), "")
    Column(
        modifier = Modifier.padding(se.westenius.theme.regularPadding),
        verticalArrangement = Arrangement.spacedBy(se.westenius.theme.regularPadding)
    ) {
        Header(text = stringResource(id = R.string.releaseDetails_notesHeader_label))
        Text(text = sanitizedNotes)
    }
}

@Composable
fun Labels(labels: List<Label>) {
    Column(
        modifier = Modifier.padding(se.westenius.theme.regularPadding),
        verticalArrangement = Arrangement.spacedBy(se.westenius.theme.regularPadding)
    ) {
        Header(text = stringResource(id = R.string.releaseDetails_labelsHeader_label))
        Text(text = labels.joinToString { it.name })
    }
}

@Composable
fun Genres(genres: List<Genre>) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Row(
            horizontalArrangement = Arrangement
                .spacedBy(se.westenius.theme.regularPadding),
            modifier = Modifier.padding(se.westenius.theme.regularPadding),
        ) {
            genres.take(3).forEach {
                GenreSticker(it.name)
            }
        }
    }
}

@Preview
@Composable
fun GenreSticker(name: String = "Techno") {
    Surface(
        shape = RoundedCornerShape(3.dp),
        border = BorderStroke(1.dp, MaterialTheme.colors.onSurface),
        color = Color.Transparent
    ) {
        Text(
            modifier = Modifier.padding(horizontal = 8.dp)
                .padding(vertical = 4.dp),
            text = name
        )
    }
}


@Composable
fun Header(
    modifier: Modifier = Modifier,
    text: String
) {
    Text(
        modifier = modifier,
        text = text,
        style = MaterialTheme.typography.h5
    )
}