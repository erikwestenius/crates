package se.westenius.wantlist

import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.text.input.TextFieldValue
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.scopes.ActivityScoped
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch
import se.westenius.discogs.collection.repository.data.Release
import se.westenius.discogs.wantlist.repository.WantlistRepository

@ExperimentalCoroutinesApi
@FlowPreview
@ActivityScoped
class WantlistViewModel @ViewModelInject constructor(
    private val repository: WantlistRepository
) : ViewModel() {

    init {
        viewModelScope.launch(Dispatchers.IO) {
            repository.update()
        }
    }

    val filterState = mutableStateOf(TextFieldValue(""))
    val filter = MutableStateFlow("")

    val wantlist: LiveData<List<Release>> =
        filter.combine(
            repository
                .wantlist
        ) { filter, wantlist ->
            wantlist.filter { release ->
                release.matchesFilter(filter)
            }
        }.distinctUntilChanged()
            .asLiveData()
}