package se.westenius.wantlist.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.map
import androidx.lifecycle.viewmodel.compose.viewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import se.westenius.common.ui.CollapsingToolbar
import se.westenius.common.ui.LoadingState
import se.westenius.common.ui.ScaffoldCollapsingToolbar
import se.westenius.common.ui.SearchBar
import se.westenius.common.ui.components.release.ReleaseHolder
import se.westenius.wantlist.R
import se.westenius.wantlist.WantlistViewModel

@FlowPreview
@ExperimentalCoroutinesApi
@Composable
fun WantlistScreen() {
    ScaffoldCollapsingToolbar(toolbar = {
        CollapsingToolbar(
            title = stringResource(id = R.string.tab_wantlist)
        )
    }) {
        WantlistContent()
    }
}

@FlowPreview
@ExperimentalCoroutinesApi
@Composable
fun WantlistContent() {
    val viewModel = viewModel<WantlistViewModel>()

    val releases by viewModel
        .wantlist
        .map { it.take(25) }
        .observeAsState(initial = emptyList())

    SearchBar(
        viewModel.filterState,
        stringResource(id = R.string.common_filterPalceHolder_label),
        modifier = Modifier.padding(horizontal = se.westenius.theme.regularPadding),
        onValueChanged = {
            viewModel.filter.value = it
        }
    )

    Column(
        modifier = Modifier.fillMaxSize()
            .padding(top = se.westenius.theme.regularPadding)
    ) {
        if (releases.isEmpty()) {
            LoadingState(Modifier.height(300.dp))
        } else {
            releases.map { release ->
                ReleaseHolder(release)
            }
        }
    }
}