package se.westenius.insights.nfs

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import dagger.hilt.android.scopes.ActivityScoped
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.take
import se.westenius.discogs.collection.repository.CollectionRepository
import se.westenius.discogs.collection.repository.data.Release

@ActivityScoped
class NotForSaleViewModel @ViewModelInject constructor(
        collectionRepository: CollectionRepository
): ViewModel() {
    val releasesCurrentlyNFS: LiveData<List<Release>> = collectionRepository.releases
            .map { releases -> releases.sortedByDescending { it.value } }
            .map { releasesByValue -> releasesByValue.filter { it.value == 0F } }
            .take(5)
            .asLiveData()
}
