package se.westenius.insights.ui

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.accompanist.pager.ExperimentalPagerApi
import se.westenius.common.navigation.Actions
import se.westenius.common.navigation.Destination
import se.westenius.common.navigation.LocalNavigator
import se.westenius.common.ui.CollapsingToolbar
import se.westenius.common.ui.ScaffoldCollapsingToolbar
import se.westenius.common.ui.components.release.EmphasizedRelease
import se.westenius.common.ui.components.release.Release
import se.westenius.common.ui.crate.Crate
import se.westenius.common.ui.crate.CrateAction
import se.westenius.common.ui.crate.CrateTitle
import se.westenius.discogs.shared.data.UpdateState
import se.westenius.insights.InsightsViewModel
import se.westenius.insights.R

val diagramHeight = 240.dp

@ExperimentalPagerApi
@Composable
fun InsightsScreen() {
    ScaffoldCollapsingToolbar(
        extraCollapsePadding = diagramHeight,
        toolbar = {
            CollapsingToolbar(
                title = stringResource(id = R.string.insights_tabTitle_label)
            )
        },
        body = {
            InsightsContent(scrollState)
        }
    )
}

@ExperimentalPagerApi
@Composable
fun InsightsContent(scrollState: ScrollState) {
    Column(
        verticalArrangement = Arrangement.spacedBy(se.westenius.theme.regularPadding)
    ) {
        InsightsChartSection(scrollState)
        UpdateProgressCard()
        //todo use viewpager instead of pager or update pager snapping
        MostValuableRecordsCrate()
        CurrentlyNotForSaleCrate()
    }
}

@Composable
fun UpdateProgressCard() {
    val updateState by viewModel<InsightsViewModel>()
        .updateState
        .observeAsState(UpdateState.Unknown)

    if (updateState == UpdateState.Unknown) {
        return
    }

    Card(
        modifier = Modifier.fillMaxWidth()
            .padding(horizontal = se.westenius.theme.regularPadding)
    ) {
        Column(modifier = Modifier.fillMaxWidth()) {
            Crossfade(targetState = updateState) { updateState ->
                Text(
                    modifier = Modifier.padding(se.westenius.theme.regularPadding),
                    text = "Updating: ${updateState.updated} of ${updateState.total} releases"
                )
            }
            LinearProgressIndicator(
                modifier = Modifier.fillMaxWidth(),
                progress = updateState.progress
            )
        }
    }
}

private val Pair<Int, Int>.progress: Float
    get() = if (second == 0) 0F
    else {
        first.toFloat() / second
    }

@Composable
fun MostValuableRecordsCrate() {
    val top5Releases by viewModel<InsightsViewModel>()
        .mostValuableReleases
        .observeAsState(initial = null)

    val navigator = LocalNavigator.current
    val actions = remember(navigator) {
        Actions(navigator)
    }

    Crate(
        title = {
            CrateTitle(
                vectorPainter = painterResource(id = R.drawable.ic_outline_leaderboard_24),
                title = stringResource(id = R.string.insights_mostValuableRecordsCrateHeader_label)
            )
        },
        action = {
            CrateAction(
                onClick = {
                    actions.toMostValuableRecordsCrate()
                },
                text = stringResource(id = R.string.crate_digThisCrate_action)
            )
        }
    ) {
        when {
            top5Releases == null -> {
                //todo loading state
            }
            top5Releases!!.isEmpty() -> {
                //todo emptyState
            }
            else -> {
                top5Releases?.let { releases ->
                    releases.forEachIndexed { index, release ->
                        if (index == 0) {
                            if (navigator.current.let {
                                    !(it is Destination.ReleaseDetail && it.release.id == release.id)
                                }) {
                                EmphasizedRelease(
                                    release = release,
                                    onClick = {
                                        actions.toReleaseDetail(release)
                                    }
                                )
                            }
                        } else {
                            Release(release = release,
                                onClick = {
                                    actions.toReleaseDetail(release)
                                }
                            )
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun CurrentlyNotForSaleCrate() {
    val releases by viewModel<InsightsViewModel>()
        .releasesCurrentlyNotForSale
        .observeAsState(initial = null)

    val navigator = LocalNavigator.current
    val actions = remember(navigator) {
        Actions(navigator)
    }

    Crate(
        title = {
            CrateTitle(
                vectorPainter = painterResource(id = R.drawable.ic_baseline_money_off_24),
                title = stringResource(id = R.string.insights_notForSaleCrate_title)
            )
        },
        action = {
            CrateAction(
                onClick = {
                    actions.toReleasesCurrentlyNFS()
                },
                text = stringResource(id = R.string.crate_digThisCrate_action)
            )
        }
    ) {
        when {
            releases == null -> {
                //todo loading state
            }
            releases!!.isEmpty() -> {
                //todo emptyState
            }
            else -> {
                releases?.let { releases ->
                    releases.forEachIndexed { index, release ->
                        if (index == 0) {
                            if (navigator.current.let {
                                    !(it is Destination.ReleaseDetail && it.release.id == release.id)
                                }) {
                                EmphasizedRelease(
                                    release = release,
                                    onClick = {
                                        actions.toReleaseDetail(release)
                                    }
                                )
                            }
                        } else {
                            Release(release = release,
                                onClick = {
                                    actions.toReleaseDetail(release)
                                }
                            )
                        }
                    }
                }
            }
        }
    }
}