package se.westenius.insights.value

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import dagger.hilt.android.scopes.ActivityScoped
import kotlinx.coroutines.flow.map
import se.westenius.discogs.collection.repository.CollectionRepository
import se.westenius.discogs.collection.repository.data.Release

@ActivityScoped
class MostValuableRecordsViewModel @ViewModelInject constructor(
    collectionRepository: CollectionRepository
): ViewModel() {
    val mostValuableRecords: LiveData<List<Release>> = collectionRepository.releases
        .map { releases -> releases.sortedByDescending { it.value } }
        .asLiveData()
}