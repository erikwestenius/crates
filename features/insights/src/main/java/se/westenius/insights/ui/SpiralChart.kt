package se.westenius.insights.ui

import androidx.compose.foundation.Canvas
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.dp

@Composable
fun SpiralChart(
    modifier: Modifier = Modifier,
    fractions: List<Float>,
    color: Color = MaterialTheme.colors.primary
) {
    val stroke = with(LocalDensity.current) { Stroke(14.dp.toPx()) }

    Canvas(modifier) {
        val halfSize = size / 2.0f
        val radius = stroke.width
        val radiusStep = stroke.width * 3f
        var size: Size
        var topLeft: Offset
        fractions.forEachIndexed { index, fraction ->
            val sweep = (fraction * 360)

            size = ((radius + radiusStep * (index + 1))).let {
                Size(it, it)
            }

            topLeft = Offset(halfSize.width - size.width / 2, halfSize.height - size.width / 2)

            drawArc(
                color = color.copy(alpha = 1.0f - index * 0.15f),
                startAngle = 0f,
                sweepAngle = sweep,
                topLeft = topLeft,
                size = size,
                useCenter = false,
                style = stroke
            )
        }
    }
}