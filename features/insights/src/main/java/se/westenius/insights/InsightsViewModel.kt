package se.westenius.insights

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import dagger.hilt.android.scopes.ActivityScoped
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import se.westenius.discogs.collection.repository.CollectionRepository
import se.westenius.discogs.collection.repository.data.Release
import se.westenius.discogs.shared.data.UpdateState
import se.westenius.insights.data.InsightChart

@ActivityScoped
class InsightsViewModel @ViewModelInject constructor(
    repository: CollectionRepository
) : ViewModel() {
    init {
        viewModelScope.launch(Dispatchers.IO) {
            repository.update()
        }
    }

    private val releasesSortedByValue: LiveData<List<Release>> = repository.releases
        .map { releases ->
            releases.sortedByDescending { it.value }
        }.asLiveData()

    val insights: LiveData<List<InsightChart>> = releasesSortedByValue.map { releases ->
        listOf(
            InsightChart.CollectionValue.create(releases = releases),
            InsightChart.LabelDistribution.create(releases = releases),
            InsightChart.GenreDistribution.create(releases = releases)
        )
    }

    val updateState: LiveData<UpdateState> = repository.collectionUpdateState.asLiveData()

    val mostValuableReleases: LiveData<List<Release>> = releasesSortedByValue.map {
        it.take(5)
    }

    val releasesCurrentlyNotForSale: LiveData<List<Release>> =
        releasesSortedByValue.map { releases ->
            releases.filter { it.value == 0F }
                .take(5)
        }
}