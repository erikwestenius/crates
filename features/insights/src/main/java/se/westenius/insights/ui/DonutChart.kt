package se.westenius.insights.ui

import androidx.compose.animation.core.*
import androidx.compose.foundation.Canvas
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.dp

private const val DividerLengthInDegrees = 1.8f

@Composable
fun DonutChart(
    modifier: Modifier = Modifier,
    fractions: List<Float>,
    color: Color = MaterialTheme.colors.primary
) {
    val stroke = with(LocalDensity.current) { Stroke(24.dp.toPx()) }

    val currentState = remember {
        MutableTransitionState(DonutAnimationState.Start)
            .apply { targetState = DonutAnimationState.End }
    }

    val transition = updateTransition(
        transitionState = currentState,
        label = "donut_enter_transition"
    )

    val angleOffset by transition.animateFloat(
        transitionSpec = {
            tween(
                delayMillis = 200,
                durationMillis = 1500
            )
        }, label = "donut_angle_offset"
    ) { state ->
        if (state == DonutAnimationState.Start) {
            0f
        } else {
            360f
        }
    }

    val shift by transition.animateFloat(
        transitionSpec = {
            tween(
                delayMillis = 200,
                durationMillis = 1500,
                easing = LinearOutSlowInEasing
            )
        }, label = "donut_shift"
    ) { state ->
        if (state == DonutAnimationState.Start) {
            -30f
        } else {
            30f
        }
    }

    val scale by transition.animateFloat(
        transitionSpec = {
            tween(
                delayMillis = 0,
                durationMillis = 1500
            )
        }, label = "donut_scale"
    ) { state ->
        if (state == DonutAnimationState.Start) {
            0.4f
        } else {
            1f
        }
    }

    Canvas(modifier) {
        val innerRadius = (size.minDimension - stroke.width) / 2
        val halfSize = size / 2.0f
        val topLeft = Offset(
            halfSize.width - innerRadius,
            halfSize.height - innerRadius
        )
        val size = Size(innerRadius * 2, innerRadius * 2)
        var startAngle = shift - 90f
        fractions.forEachIndexed { index, fraction ->
            val sweep = fraction * angleOffset
            drawArc(
                color = color.copy(alpha = 1.0f - index * 0.15f),
                startAngle = startAngle + DividerLengthInDegrees / 2,
                sweepAngle = sweep - DividerLengthInDegrees,
                topLeft = topLeft,
                size = size * scale,
                useCenter = false,
                style = stroke
            )
            startAngle += sweep
        }
    }
}

private enum class DonutAnimationState { Start, End }