package se.westenius.insights.ui

import androidx.annotation.DrawableRes
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.Image
import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.TransformOrigin
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.PagerState
import se.westenius.common.asFormattedPrice
import se.westenius.common.ui.LoadingState
import se.westenius.insights.InsightsViewModel
import se.westenius.insights.R
import se.westenius.insights.data.InsightChart
import kotlin.math.abs
import kotlin.math.roundToLong

@ExperimentalPagerApi
@Composable
fun InsightsChartSection(scrollState: ScrollState) {

    val maxOffset = with(LocalDensity.current) {
        240.dp.toPx()
    }

    val scrollAnimationProgress = (1 - abs(scrollState.value / maxOffset))
        .coerceAtLeast(0f)

    val insights by viewModel<InsightsViewModel>()
        .insights
        .observeAsState(emptyList())

//    if (insights.isEmpty()) {
//        InsightsChartLoadingState(scrollAnimationProgress) //todo extract scrollProgress to state
//    }else {
    Column(
        modifier = Modifier
            .fillMaxWidth()
    ) {
        InsightsChart(
            modifier = Modifier
                .graphicsLayer(
                    scaleX = scrollAnimationProgress,
                    scaleY = scrollAnimationProgress,
                    transformOrigin = TransformOrigin(0.5F, 1.0F)
                ),
//            pagerState = pagerState,
            insights = insights
        )
//        InsightsPagerIndicator(
//            modifier = Modifier.padding(vertical = se.westenius.theme.regularPadding),
//            pagerState,
//            insights
//        )
        InsightsLegend(
//            pagerState = pagerState,
            insights = insights
        )
    }
//    }
}

//@Composable
//fun InsightsChartLoadingState(
//    scrollAnimationProgress: Float
//) {
//    Column(
//        modifier = Modifier
//            .fillMaxWidth(),
//        horizontalAlignment = Alignment.CenterHorizontally
//    ) {
//        Box(
//            gravity = ContentGravity.Center,
//            modifier = Modifier.size(200.dp)
//                .drawLayer(
//                    scaleX = scrollAnimationProgress,
//                    scaleY = scrollAnimationProgress,
//                    transformOrigin = TransformOrigin(0.5F, 1.0F)
//                )
//        ){
//            CircularProgressIndicator()
//        }
////        InsightsLegend(
////            pagerState = pagerState,
////            insights = insights
////        )
//    }
//}

@Composable
fun InsightsChart(
    modifier: Modifier,
//    pagerState: PagerState,
    insights: List<InsightChart>
) {
    val currentPage = 0
    Box(
        modifier = modifier
            .height(diagramHeight)
            .fillMaxWidth(),
        contentAlignment = Alignment.Center
    ) {
        if (insights.isEmpty()) {
            LoadingState(Modifier.height(400.dp))
        } else {
            //todo, fix this
//            Crossfade(targetState = currentPage) { currentPage ->
                when (val insight = insights[currentPage]) {
                    is InsightChart.CollectionValue -> {
                        CollectionValueInsightChart(insight = insight)
                    }
                    is InsightChart.LabelDistribution -> {
                        LabelDistributionChart(insight = insight)
                    }
                    is InsightChart.GenreDistribution -> {
                        GenreDistributionChart(insight = insight)
                    }
//                }
            }
        }
    }
}

@Composable
fun CollectionValueInsightChart(
    insight: InsightChart.CollectionValue
) {
    DonutChart(
        modifier = Modifier.size(200.dp),
        fractions = insight.fractions
    )
}

@Composable
fun LabelDistributionChart(
    insight: InsightChart.LabelDistribution
) {
    val maxRecordsPerLabel = insight.recordsPerLabel.maxOf { it.second }
    val fractions = insight.recordsPerLabel.map { (_, count) ->
        count.toFloat() / maxRecordsPerLabel
    }

    BarChart(
        modifier = Modifier.size(200.dp),
        fractions = fractions
    )
}

@Composable
fun GenreDistributionChart(
    insight: InsightChart.GenreDistribution
) {
    SpiralChart(
        modifier = Modifier.size(200.dp),
        fractions = insight.fractions.map { it.second },
    )
}

//@Composable
//fun InsightsPagerIndicator(
//    modifier: Modifier = Modifier,
//    pagerState: PagerState,
//    insights: List<InsightChart>
//) {
//    PagerIndicator(
//        modifier = modifier,
//        pagerSate = pagerState,
//        icon = { iconModifier, page ->
//            Image(
//                modifier = iconModifier,
//                painter = painterResource(
//                    id = if (insights.isEmpty()) {
//                        R.drawable.ic_baseline_donut_large_12 //todo make icon nullable..
//                    } else {
//                        insights[page].iconRes
//                    }
//
//                ),
//                colorFilter = ColorFilter.tint(MaterialTheme.colors.primary),
//                contentDescription = null
//            )
//        }
//    )
//}

private val InsightChart.iconRes: Int
    @DrawableRes
    get() = when (this) {
        is InsightChart.GenreDistribution -> R.drawable.ic_spiral_chart_12
        is InsightChart.LabelDistribution -> R.drawable.ic_baseline_bar_chart_12
        is InsightChart.CollectionValue -> R.drawable.ic_baseline_donut_large_12
    }

@ExperimentalPagerApi
@Composable
fun InsightsLegend(
    modifier: Modifier = Modifier,
    insights: List<InsightChart>
) {
    val pagerState = remember { PagerState(pageCount = insights.size) }

    Column {
        if (insights.isEmpty()) {
            Card(modifier.fillMaxSize()) {
                LoadingState(Modifier.height(400.dp))
            }
        } else {
            HorizontalPager(
                modifier = Modifier.height(160.dp),
                state = pagerState,
                offscreenLimit = 2
            ) { page ->
                when (val insight = insights[page]) {
                    is InsightChart.CollectionValue -> {
                        CollectionValueLegend(insight = insight)
                    }
                    is InsightChart.LabelDistribution -> {
                        LabelDistributionLegend(insight = insight)
                    }
                    is InsightChart.GenreDistribution -> {
                        GenreDistributionLegend(insight = insight)
                    }
                }
            }
        }
    }
}

@Composable
fun CollectionValueLegend(
    insight: InsightChart.CollectionValue
) {
    LegendCard(
        title = stringResource(id = R.string.insights_valueDistributionInsightTitle_label),
        subtitle = "${insight.numberOfRecords} records worth ${insight.totalValue.asFormattedPrice()}" //todo extract to format
    ) {
        with(insight.ranges) {
            for (i in indices step 2) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                ) {
                    for (j in i..i + 1) {
                        LegendEntry(
                            color = MaterialTheme.colors.primary.copy(alpha = 1.0f - (j * 0.15f)),
                            label = get(j).let { (range, count) ->
                                if (range.second == Float.MAX_VALUE) {
                                    "${range.first.asFormattedPrice()} +: $count"
                                } else {
                                    "${range.first.roundToLong()}-${range.second.asFormattedPrice()}: $count"
                                }
                            }
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun LabelDistributionLegend(insight: InsightChart.LabelDistribution) {
    if (insight.recordsPerLabel.size < 4) {
        return
    }
    LegendCard(
        title = stringResource(id = R.string.insights_labelDistributionInsightTitle_label),
        subtitle = "${insight.numberOfLabels} labels" //todo extract format
    ) {
        with(insight.recordsPerLabel) {
            for (i in indices step 2) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                ) {
                    for (j in i..i + 1) {
                        LegendEntry(
                            color = MaterialTheme.colors.primary.copy(alpha = 1.0f - (j * 0.15f)),
                            label = get(j).let { (label, count) ->
                                "$count: $label"
                            }
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun GenreDistributionLegend(insight: InsightChart.GenreDistribution) {
    if (insight.fractions.size < 4) {
        return
    }
    LegendCard(
        title = stringResource(id = R.string.insights_genreDistributionInsightTitle_label),
        subtitle = "${insight.numberOfGenres} genres" //todo extract format
    ) {
        with(insight.fractions) {
            for (i in indices step 2) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                ) {
                    for (j in i..i + 1) {
                        LegendEntry(
                            color = MaterialTheme.colors.primary.copy(alpha = 1.0f - (j * 0.15f)),
                            label = get(j).let { (genre, percentage) ->
                                "${(percentage * 100f).roundToLong()}%: $genre"
                            }
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun LegendCard(
    modifier: Modifier = Modifier,
    title: String,
    subtitle: String,
    content: @Composable () -> Unit
) {
    Card(
        modifier = modifier
            .padding(horizontal = se.westenius.theme.regularPadding)
            .fillMaxWidth(),
        elevation = 2.dp
    ) {
        Column(
            modifier = Modifier
                .padding(se.westenius.theme.regularPadding)
                .fillMaxWidth(),
            verticalArrangement = Arrangement.spacedBy(se.westenius.theme.smallPadding)
        ) {
            Text(
                text = title,
                style = MaterialTheme.typography.h4
            )
            Text(
                text = subtitle,
                style = MaterialTheme.typography.caption
            )
            content()
        }
    }
}

@Composable
fun LegendEntry(
    color: Color,
    label: String
) {
    Row(
        horizontalArrangement = Arrangement.spacedBy(se.westenius.theme.smallPadding),
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .padding(end = 4.dp)
    ) {
        Box(
            modifier = Modifier
                .size(16.dp)
                .background(color, shape = MaterialTheme.shapes.small),
        )
        Text(
            text = label,
            style = MaterialTheme.typography.body1,
            fontWeight = FontWeight.Medium,
            overflow = TextOverflow.Ellipsis,
            maxLines = 1
        )
    }
}