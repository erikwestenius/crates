package se.westenius.insights.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.ButtonDefaults.IconSize
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@Composable
fun PagerIndicator(
    modifier: Modifier = Modifier,
//    pagerSate: PagerState,
    icon: @Composable (Modifier, Int) -> Unit,
    sizeIncludingPadding: Dp = 12.dp
) {
    val indicatorSizePx = with(LocalDensity.current) {
        sizeIncludingPadding.toPx()
    }
    Box(
        modifier = modifier
            .height(sizeIncludingPadding)
            .fillMaxWidth()
    ) {
//        (pagerSate.minPage..pagerSate.maxPage).map { page ->
//            val translation = (-indicatorSizePx * 3) / 2 + indicatorSizePx * page
//            val selected = pagerSate.currentPage == page
//            Indicator(
//                modifier = Modifier
//                    .align(alignment = Alignment.Center)
//                    .graphicsLayer(translationX = translation),
//                selected = selected,
//                icon = { modifier ->
//                    icon(modifier, page) //todo investigate scopes!?
//                }
//            )
//        }
    }
}

@Composable
private fun Indicator(
    modifier: Modifier = Modifier,
    selected: Boolean,
    icon: @Composable ((Modifier) -> Unit)
) {

    Box(
        modifier = modifier
            .size(5.dp)
            .background(MaterialTheme.colors.primary, CircleShape)
    ) {
        icon.invoke(modifier.size(IconSize))
    }
}

private enum class IndicatorAnimationState {
    Unselected,
    Selected
}
