package se.westenius.insights.ui

import androidx.compose.foundation.Canvas
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.dp

@Composable
fun BarChart(
    modifier: Modifier = Modifier,
    fractions: List<Float>,
    color: Color = MaterialTheme.colors.primary
) {
    val strokeWidth = with(LocalDensity.current) { 32.dp.toPx() }

    Canvas(modifier) {
        var x = strokeWidth / 2
        val step = strokeWidth + ((size.width - strokeWidth * 4) / (fractions.size - 1))
        val height = size.height
        fractions.forEachIndexed { index, fraction ->
            drawLine(
                color = color.copy(alpha = 1.0f - index * 0.15f),
                start = Offset(x, height),
                end = Offset(x, height - (height * fraction)),
                strokeWidth = strokeWidth
            )
            x += step
        }
    }
}

private enum class BarChartAnimationState { Start, End }