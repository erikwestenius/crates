package se.westenius.insights.nfs

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.map
import androidx.lifecycle.viewmodel.compose.viewModel
import se.westenius.common.ui.CollapsingToolbar
import se.westenius.common.ui.ScaffoldCollapsingToolbar
import se.westenius.common.ui.components.release.ReleaseHolder
import se.westenius.insights.R

@Composable
fun NotForSaleScreen() {
    ScaffoldCollapsingToolbar(toolbar = {
        CollapsingToolbar(
            title = stringResource(id = R.string.insights_notForSale_title),
            showBackButton = true
        )
    }) {
        NotForSaleContent()
    }
}

@Composable
fun NotForSaleContent() {
    val records by viewModel<NotForSaleViewModel>()
            .releasesCurrentlyNFS
            .map { it.take(100) }
            .observeAsState(emptyList())

    records.forEach {release ->
        ReleaseHolder(release = release)
    }
}