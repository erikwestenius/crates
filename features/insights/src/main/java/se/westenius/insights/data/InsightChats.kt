package se.westenius.insights.data

import se.westenius.discogs.collection.repository.data.Release
import kotlin.math.roundToLong

sealed class InsightChart {

    data class CollectionValue(
        val fractions: List<Float>,
        val ranges: List<Pair<Pair<Float, Float>, Int>>,
        val numberOfRecords: Int,
        val totalValue: Long
    ) : InsightChart() {

        companion object {
            private val RANGES = listOf(
                (0F..49.999F),
                (50F..99.999F),
                (100F..199.99F),
                (200F..Float.MAX_VALUE)
            )

            fun create(releases: List<Release>): CollectionValue {
                val numberOfRecords = releases.size
                val totalValue = releases.map { it.value }.sum().roundToLong()

                val ranges = releases.map { it.value }
                    .let { values ->
                        RANGES.map { range ->
                            Pair(
                                Pair(range.start, range.endInclusive),
                                values.filter { range.contains(it) }.count()
                            )
                        }
                    }

                val fractions = ranges.map { (_, count) ->
                    count.toFloat() / numberOfRecords
                }

                return CollectionValue(
                    fractions = fractions,
                    ranges = ranges,
                    numberOfRecords = numberOfRecords,
                    totalValue = totalValue
                )
            }
        }
    }

    data class GenreDistribution(
        val fractions: List<Pair<String, Float>>,
        val numberOfGenres: Int
    ) : InsightChart() {

        companion object {
            fun create(releases: List<Release>): GenreDistribution {
                val totalReleases = releases.count()

                val genres = releases.flatMap { release ->
                    release.genres.map { Pair(it.name, release.id) }
                }.groupBy { (genre, _) -> genre }

                val numberOfGenres = genres.count()

                val fractions = genres
                    .map { (genre, releases) ->
                        Pair(genre, releases.count().toFloat() / totalReleases)
                    }.sortedByDescending { (_, percentage) -> percentage }
                    .take(4)
                return GenreDistribution(
                    fractions = fractions,
                    numberOfGenres = numberOfGenres
                )
            }
        }
    }

    data class LabelDistribution(
        val recordsPerLabel: List<Pair<String, Int>>,
        val numberOfLabels: Int
    ) : InsightChart() {
        companion object {
            fun create(releases: List<Release>): LabelDistribution {
                val labels = releases.flatMap { release ->
                    release.labels.map { label ->
                        Pair(label.name, release.id)
                    }
                }
                val recordsPerLabel = labels
                    .groupBy { (label, _) ->
                        label
                    }.map { (label, recordIds) ->
                        Pair(label, recordIds.count())
                    }.sortedByDescending { (_, count) ->
                        count.toFloat()
                    }.take(4)

                val numberOfLabels = labels.map { (label, _) ->
                    label
                }.toSet().size

                return LabelDistribution(
                    recordsPerLabel = recordsPerLabel,
                    numberOfLabels = numberOfLabels
                )
            }
        }
    }
}