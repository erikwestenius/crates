# Crater
Crater is backed by the discogs.com api. Discogs is the de facto standard for cataloging your vinyl collection and it is also by far the biggest marketplace for second hand vinyl.

Our app utilises this api to pull down and sort your collection,hence you will need a Discogs account to try the app out. Ask one of us if you don't have a collection and want a demo. It also pulls down the price of each record periodically once every 48h.

Work done:
* Erik: 20h compose 20h architecture
* Hugo: 8h
* Emyr: 4h
* Robin: QA
