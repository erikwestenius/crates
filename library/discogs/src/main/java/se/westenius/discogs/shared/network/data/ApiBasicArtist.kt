package se.westenius.discogs.shared.network.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiBasicArtist(
    val id: Long,
    val name: String,
    @Json(name = "resource_url") val resourceUrl: String
)