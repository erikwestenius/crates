package se.westenius.discogs.wantlist.repository

import kotlinx.coroutines.flow.Flow
import se.westenius.discogs.BaseRepository
import se.westenius.discogs.collection.repository.data.Release
import se.westenius.discogs.wantlist.network.WantlistApiService
import se.westenius.discogs.wantlist.persistence.WantlistDatabase
import javax.inject.Inject
import javax.inject.Singleton

interface WantlistRepository {
    val wantlist: Flow<List<Release>>
    suspend fun update()
}

@Singleton
class WantlistRepositoryImpl @Inject constructor(
    private val apiService: WantlistApiService,
    private val database: WantlistDatabase
) : BaseRepository(), WantlistRepository {

    override val wantlist: Flow<List<Release>> = database.wantlist

    override suspend fun update() = withPagination(
        request = { page ->
            apiService.getWantlist(page)
        },
        response = {
            database.insert(it)
        }
    )
}
