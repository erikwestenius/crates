package se.westenius.discogs.shared.persistence.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import se.westenius.discogs.shared.persistence.entity.LabelEntity

@Dao
interface LabelDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entity: LabelEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entities: List<LabelEntity>)

    @Query("SELECT * FROM LabelEntity")
    fun getAll(): Flow<List<LabelEntity>>
}