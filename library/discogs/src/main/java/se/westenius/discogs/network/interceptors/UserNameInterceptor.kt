package se.westenius.discogs.network.interceptors

import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.Response
import se.westenius.discogs.network.NetworkPreferences
import javax.inject.Inject
import javax.inject.Singleton

private const val USER_NAME_PLACEHOLDER = "{username}"

@Singleton
class UserNameInterceptor @Inject constructor(
    private val preferences: NetworkPreferences
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var userName = runBlocking { preferences.userName.first() }
        userName = if (userName.isEmpty()) {
            runBlocking { preferences.userName.first() }
        }else{
            userName
        }

        return if (chain.request().url().pathSegments().contains(USER_NAME_PLACEHOLDER)) {
            val url = chain.request().url()
                .let {
                    val builder = it.newBuilder()
                    val segments = it.pathSegments()
                    segments.forEachIndexed { index, segment ->
                        if (segment == USER_NAME_PLACEHOLDER) {
                            builder.setPathSegment(index, userName)
                        }
                    }
                    builder.build()
                }
            chain.proceed(
                chain.request()
                    .newBuilder()
                    .url(url)
                    .build()
            )
        } else {
            chain.proceed(chain.request())
        }
    }
}