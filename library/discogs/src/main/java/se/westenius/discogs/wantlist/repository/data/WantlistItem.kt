package se.westenius.discogs.wantlist.repository.data

data class WantlistItem(
    val id: Long,
    val thumbnailUrl: String,
    val title: String
)