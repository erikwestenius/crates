package se.westenius.discogs.shared.network.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiBasicFormat(
    val qty: String,
    val descriptions: List<String>,
    val name: String
)