package se.westenius.discogs.search

import se.westenius.discogs.BaseRepository
import se.westenius.discogs.network.Result
import javax.inject.Inject
import javax.inject.Singleton

interface SearchRepository {
    suspend fun search(query: String, type: SearchType): Result<GetSearchResult>
    suspend fun getArtist(id: Long): Result<GetArtistResult>
    suspend fun getLabel(id: Long): Result<GetLabelResult>
}

@Singleton
class SearchRepositoryImpl @Inject constructor(
    val searchApiService: SearchApiService
) : BaseRepository(), SearchRepository {

    override suspend fun search(
        query: String,
        type: SearchType
    ): Result<GetSearchResult> {
        return searchApiService.search(query, type)
    }

    override suspend fun getArtist(id: Long): Result<GetArtistResult> {
        return searchApiService.getArtist(id)
    }

    override suspend fun getLabel(id: Long): Result<GetLabelResult> {
        return searchApiService.getLabel(id)
    }
}

enum class SearchType {
    ARTIST, RELEASE, LABEL
}
