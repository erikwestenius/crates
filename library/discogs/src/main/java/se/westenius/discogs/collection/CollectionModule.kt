package se.westenius.discogs.collection

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import se.westenius.discogs.collection.persistence.CollectionDatabase
import se.westenius.discogs.collection.persistence.CollectionDatabaseImpl
import se.westenius.discogs.collection.repository.CollectionRepository
import se.westenius.discogs.collection.repository.CollectionRepositoryImpl

@Module
@InstallIn(ApplicationComponent::class)
abstract class CollectionModule {
    @Binds
    abstract fun bindCollectionRepository(
        impl: CollectionRepositoryImpl
    ) : CollectionRepository

    @Binds
    abstract fun bindCollectionDatabase(
        impl: CollectionDatabaseImpl
    ) : CollectionDatabase
}