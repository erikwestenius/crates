package se.westenius.discogs.wantlist.persistence.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import se.westenius.discogs.wantlist.persistence.entity.WantlistRelease
import se.westenius.discogs.wantlist.persistence.entity.WantlistReleaseEntity

@Dao
interface WantlistDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(items: List<WantlistReleaseEntity>)

    @Query("SELECT * FROM WantlistReleaseEntity")
    fun getAll(): Flow<List<WantlistRelease>>
}