package se.westenius.discogs.shared.persistence.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import se.westenius.discogs.shared.persistence.entity.PopulatedRelease
import se.westenius.discogs.shared.persistence.entity.ReleaseEntity
import se.westenius.discogs.shared.persistence.entity.ReleaseUpdateFlag

@Dao
interface ReleaseDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(items: List<ReleaseEntity>)

    @Query("SELECT * FROM ReleaseEntity WHERE releaseId=:id")
    fun get(id: Long) : Flow<PopulatedRelease>

    @Query("SELECT releaseId, dirty FROM ReleaseEntity")
    fun getCurrentUpdateState(): List<ReleaseUpdateFlag>

    @Query("SELECT releaseId, dirty FROM ReleaseEntity")
    fun getUpdateState(): Flow<List<ReleaseUpdateFlag>>

    @Query("UPDATE ReleaseEntity SET dirty=:dirty WHERE releaseId=:id")
    fun updateDirty(id: Long, dirty: Boolean)

    @Query("UPDATE ReleaseEntity SET dirty=:dirty")
    fun updateAllDirty(dirty: Boolean)
}