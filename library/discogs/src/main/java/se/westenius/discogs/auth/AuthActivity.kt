package se.westenius.discogs.auth

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Button
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import se.westenius.discogs.R
import se.westenius.theme.CraterTheme
import timber.log.Timber

@FlowPreview
@ExperimentalCoroutinesApi
@AndroidEntryPoint
class AuthActivity : ComponentActivity() {

    private val viewModel: AuthViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.isAuthorized.observe(this) { authorized ->
            if (authorized) {
                finish()
            }
        }

        intent.data?.let { uri ->
            lifecycleScope.launchWhenCreated {
                try {
                    viewModel.verifyAuthToken(uri.asAuthorizedToken())
                } catch (e: Exception) {
                    Timber.w("Something went wrong when verifying token!")
                }
            }
        }

        setContent {
            CraterTheme {
                Box(
                    modifier = Modifier.fillMaxSize(),
                ) {
                    Auth()
                }
            }
        }
    }

    @Composable
    fun Auth() {
        val isLoading by viewModel.isLoading.observeAsState()
        Crossfade(targetState = isLoading) {
            if (isLoading == true) {
                Loading()
            } else {
                Login()
            }
        }
    }

    @Composable
    fun Login() {
        Button(
            onClick = ::authorize,
            modifier = Modifier.background(color = Color(0xFF212529))
        ) {
            Text(text = getString(R.string.auth_login_action))
        }
    }

    @Composable
    fun Loading() {
        CircularProgressIndicator()
    }

    private fun authorize() {
        viewModel.authorize { response ->
            startActivity(
                Intent(Intent.ACTION_VIEW).apply {
                    data = response.authUri
                }
            )
        }
    }

    companion object {
        fun createIntent(context: Context) = Intent(context, AuthActivity::class.java)
    }
}

private const val TOKEN_PLACEHOLDER = "{TOKEN}"
private const val AUTH_URL = "https://discogs.com/oauth/authorize?oauth_token=$TOKEN_PLACEHOLDER"

private val AuthTokenResponse.authUri: Uri
    get() = Uri.parse(AUTH_URL.replace(TOKEN_PLACEHOLDER, oauthToken))


private const val QUERY_AUTH_TOKEN = "oauth_token"
private const val QUERY_AUTH_VERIFIER = "oauth_verifier"

private fun Uri.asAuthorizedToken() = VerifiedToken(
    oauthToken = getQueryParameter(QUERY_AUTH_TOKEN)!!,
    oauthVerifier = getQueryParameter(QUERY_AUTH_VERIFIER)!!
)