package se.westenius.discogs.shared.network.data

import com.squareup.moshi.JsonClass

interface PaginatedResponse {
    val pagination: ApiPagination
}

@JsonClass(generateAdapter = true)
data class ApiPagination (
    val per_page: Int,
    val pages: Int,
    val page: Int,
    val items: Int
)