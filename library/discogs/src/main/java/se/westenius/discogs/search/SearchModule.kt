package se.westenius.discogs.search

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent

@Module
@InstallIn(ApplicationComponent::class)
abstract class SearchModule {
    @Binds
    abstract fun bindSearchRepository(
        impl: SearchRepositoryImpl
    ) : SearchRepository
}