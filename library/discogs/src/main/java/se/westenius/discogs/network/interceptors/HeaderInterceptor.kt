package se.westenius.discogs.network.interceptors

import android.os.SystemClock
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.Response
import se.westenius.discogs.network.NetworkPreferences
import java.time.Instant
import javax.inject.Inject
import javax.inject.Singleton

const val CONSUMER_KEY = "cDnGUMlMvVphFTZnZzNN"
const val CONSUMER_SECRET = "hCyEzgNXArEmTaRYdsJkNCdEdFYPrtSu"

@Singleton
class HeaderInterceptor @Inject constructor(
    private val preferences: NetworkPreferences
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val token = runBlocking { preferences.authToken.first() }
        val secret = runBlocking { preferences.authSecret.first() }

        return chain.proceed(
            chain.request()
                .newBuilder()
                .addHeader("User-Agent", "crates")
                .addHeader("Content-Type", "application/json")
                .addHeader(
                    "Authorization",
                    "OAuth oauth_consumer_key=\"$CONSUMER_KEY\"," +
                            " oauth_nonce=\"${SystemClock.uptimeMillis()}\"," +
                            " oauth_token=\"$token\"," +
                            " oauth_signature=\"$CONSUMER_SECRET&$secret\"," +
                            " oauth_signature_method=\"PLAINTEXT\"," +
                            " oauth_version=1.0," +
                            " oauth_timestamp=\"${Instant.now().toEpochMilli()}\""
                ).build()
        )
    }
}