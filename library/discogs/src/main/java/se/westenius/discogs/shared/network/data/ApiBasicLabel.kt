package se.westenius.discogs.shared.network.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiBasicLabel(
    val catno : String,
    val id: Long,
    val name: String,
    @Json(name = "entity_type") val entityType: String,
    @Json(name="resource_url")val resourceUrl: String
)