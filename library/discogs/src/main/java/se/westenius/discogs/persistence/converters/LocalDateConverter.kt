package se.westenius.discogs.persistence.converters

import androidx.room.TypeConverter
import java.time.LocalDate

class LocalDateConverter {
    @TypeConverter
    fun toDate(dateString: String?): LocalDate? =
        dateString?.let {
            LocalDate.parse(it)
        }

    @TypeConverter
    fun toDateString(date: LocalDate?): String? = date?.toString()
}