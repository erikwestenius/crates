package se.westenius.discogs

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import se.westenius.discogs.collection.persistence.dao.CollectionDao
import se.westenius.discogs.persistence.DiscogsDatabase
import se.westenius.discogs.shared.persistence.dao.*
import se.westenius.discogs.wantlist.persistence.dao.WantlistDao
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
internal object DiscogsDatabaseModule {
    @Singleton
    @Provides
    fun provideDatabase(
        @ApplicationContext context: Context
    ) : DiscogsDatabase = Room.databaseBuilder(
        context,
        DiscogsDatabase::class.java,
        "discogs_database"
    ).enableMultiInstanceInvalidation()
        .build()

    @Singleton
    @Provides
    fun provideCollectionDao(
        database: DiscogsDatabase
    ): CollectionDao = database.collectionDao()

    @Singleton
    @Provides
    fun provideWantlistDao(
        database: DiscogsDatabase
    ): WantlistDao = database.wantlistDao()

    @Singleton
    @Provides
    fun provideCollectionReleaseDao(
        database: DiscogsDatabase
    ): ReleaseDao = database.releaseDao()

    @Singleton
    @Provides
    fun provideCollectionReleaseValueDao(
        database: DiscogsDatabase
    ): ReleaseValueDao = database.releaseValueDao()

    @Singleton
    @Provides
    fun provideCollectionArtistDao(
        database: DiscogsDatabase
    ): ArtistDao = database.artistDao()

    @Singleton
    @Provides
    fun provideCollectionLabelDao(
        database: DiscogsDatabase
    ): LabelDao = database.labelDao()

    @Singleton
    @Provides
    fun provideCollectionFormatDao(
        database: DiscogsDatabase
    ): FormatDao = database.formatDao()

    @Singleton
    @Provides
    fun provideGenreDao(
        database: DiscogsDatabase
    ): GenreDao = database.genreDao()
}