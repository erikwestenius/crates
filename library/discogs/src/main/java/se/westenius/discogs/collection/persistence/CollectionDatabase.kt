package se.westenius.discogs.collection.persistence

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import se.westenius.discogs.collection.network.GetCollectionResponse
import se.westenius.discogs.collection.persistence.dao.CollectionDao
import se.westenius.discogs.collection.persistence.entity.CollectedRelease
import se.westenius.discogs.collection.persistence.entity.CollectedReleaseEntity
import se.westenius.discogs.collection.repository.data.Artist
import se.westenius.discogs.collection.repository.data.Genre
import se.westenius.discogs.collection.repository.data.Label
import se.westenius.discogs.collection.repository.data.Release
import se.westenius.discogs.shared.persistence.SharedDatabase
import se.westenius.discogs.shared.persistence.entity.ArtistEntity
import se.westenius.discogs.shared.persistence.entity.GenreEntity
import se.westenius.discogs.shared.persistence.entity.LabelEntity
import javax.inject.Inject
import javax.inject.Singleton

interface CollectionDatabase {
    val collectionItems: Flow<List<Release>>
    fun insert(response: GetCollectionResponse)
}

@Singleton
class CollectionDatabaseImpl @Inject constructor(
    private val sharedDatabase: SharedDatabase,
    private val collectionDao: CollectionDao
) : CollectionDatabase {

    //todo handle removed releases, right now they will be kept in the db..
    override val collectionItems: Flow<List<Release>>
        get() = collectionDao.collectedReleases().map { entities ->
            entities.map { entity ->
                entity.asRelease()
            }
        }

    override fun insert(response: GetCollectionResponse) {
        sharedDatabase.insert(response.items.map { it.basicInformation })
        insertCollectedReleases(response)
    }

    private fun insertCollectedReleases(response: GetCollectionResponse) {
        response.items.map {
            CollectedReleaseEntity(it.id)
        }.let {
            collectionDao.insert(it)
        }
    }
}

private fun CollectedRelease.asRelease() =
    Release(
        id = release.releaseId,
        thumbnailUrl = release.releaseThumbnailUrl,
        title = release.releaseTitle,
        artists = releaseArtists.map { it.asArtist() },
        labels = releaseLabels.map { it.asLabel() },
        genres = genres.map { it.asGenre() },
        value = if (releaseValue.isNotEmpty()) {
            releaseValue[0].value
        } else 0F
    )

internal fun ArtistEntity.asArtist() = Artist(
    id = artistId,
    name = artistName
)

internal fun LabelEntity.asLabel() = Label(
    id = labelId,
    name = labelName
)

internal fun GenreEntity.asGenre() = Genre(
    name = genreName
)