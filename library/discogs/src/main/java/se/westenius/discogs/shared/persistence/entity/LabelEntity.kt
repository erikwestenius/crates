package se.westenius.discogs.shared.persistence.entity

import androidx.room.Entity

@Entity(
    primaryKeys = [
        "labelId",
        "labelReleaseId"
    ]
)
data class LabelEntity(
    val labelId: Long,
    val labelReleaseId: Long,
    val labelCatno: String,
    val labelName: String,
    val labelEntityType: String,
    val labelResourceUrl: String
)