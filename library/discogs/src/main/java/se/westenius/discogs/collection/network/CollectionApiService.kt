package se.westenius.discogs.collection.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import se.westenius.discogs.network.BaseApiService
import se.westenius.discogs.network.Result
import se.westenius.discogs.shared.network.data.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CollectionApiService @Inject constructor(
    retrofit: Retrofit
) : BaseApiService<CollectionApiService.Api>(
    retrofit,
    Api::class.java
) {

    suspend fun getCollection(page: Int = 0) =
        apiCall {
            api.getCollection(page)
        }

    suspend fun getReleaseInfo(id: Long): Result<GetReleaseInfo> =
        apiCall {
            api.getReleaseInfo(id)
        }

    interface Api {
        @GET("/users/{username}/collection/folders/0/releases") //0 is all releases
        suspend fun getCollection(
            @Query("page") page: Int,
            @Query("per_page") perPage: Int = 100, //100 is max
        ): GetCollectionResponse

        @GET("/releases/{release_id}?SEK")
        suspend fun getReleaseInfo(
            @Path("release_id") id: Long
        ): GetReleaseInfo
    }
}

@JsonClass(generateAdapter = true)
data class GetCollectionResponse(
    override val pagination: ApiPagination,
    @Json(name = "releases") val items: List<ApiRelease>
) : PaginatedResponse

@JsonClass(generateAdapter = true)
data class ApiRelease(
    val id: Long,
    val rating: Int,
    @Json(name = "basic_information") val basicInformation: ApiBasicRelease
)

@JsonClass(generateAdapter = true)
data class GetReleaseInfo(
    val id: Long,
    val title: String,
    val community: ApiReleaseCommunityInfo,
    val thumb: String?,
    val genres: List<String>?,
    val styles: List<String>?,
    val labels: List<ApiBasicLabel>?,
    val artists: List<ApiBasicArtist>?,
    @Json(name = "lowest_price") val value: Float?,
    @Json(name = "num_for_sale") val numForSale: Int?,
    val notes: String?,
    @Json(name = "tracklist") val trackList: List<ApiTrack>?,
    val videos: List<ApiReleaseVideo>?,
    val year: Int?
)

@JsonClass(generateAdapter = true)
data class ApiReleaseCommunityInfo(
    val have: Int,
    val want: Int
)

@JsonClass(generateAdapter = true)
data class ApiTrack(
    val duration: String,
    val position: String,
    val title: String
)

@JsonClass(generateAdapter = true)
data class ApiReleaseVideo(
    val description: String,
    val duration: Int,
    val title: String,
    val uri: String
)