package se.westenius.discogs.shared.persistence.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import se.westenius.discogs.shared.persistence.entity.FormatEntity

@Dao
interface FormatDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entities: List<FormatEntity>)

    @Query("SELECT * FROM FormatEntity")
    fun getAll(): Flow<List<FormatEntity>>
}