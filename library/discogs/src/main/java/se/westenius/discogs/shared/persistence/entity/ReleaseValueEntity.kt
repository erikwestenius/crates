package se.westenius.discogs.shared.persistence.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ReleaseValueEntity (
    @PrimaryKey val valueReleaseId: Long,
    val value: Float,
    val numForSale: Int,
    val have: Int,
    val want: Int
)