package se.westenius.discogs.shared.persistence.entity

import androidx.room.Entity

@Entity(
    primaryKeys = [
        "genreName",
        "genreReleaseId"
    ]
)
data class GenreEntity(
    val genreName: String,
    val genreReleaseId: Long,
)