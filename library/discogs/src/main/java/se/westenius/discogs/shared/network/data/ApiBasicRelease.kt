package se.westenius.discogs.shared.network.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiBasicRelease(
    val id: Long,
    val formats: List<ApiBasicFormat>,
    val title: String,
    val year: Int,
    val labels: List<ApiBasicLabel>,
    val artists: List<ApiBasicArtist>,
    @Json(name = "thumb") val thumbnailUrl: String,
    @Json(name = "cover_image") val coverImageUrl: String
)