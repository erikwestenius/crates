package se.westenius.discogs.user

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import retrofit2.Retrofit
import retrofit2.http.GET
import se.westenius.discogs.network.BaseApiService
import javax.inject.Inject

class UserApiService @Inject constructor(
    retrofit: Retrofit
) : BaseApiService<UserApiService.Api>(
    retrofit,
    Api::class.java
) {

    suspend fun getIdentity() = apiCall {
        api.getIdentity()
    }

    interface Api {
        @GET("oauth/identity")
        suspend fun getIdentity(): User
    }
}

@JsonClass(generateAdapter = true)
data class User(
    val id: Int,
    val username: String,
    @Json(name = "resource_url") val resourceUrl: String,
    @Json(name = "consumer_name") val consumerName: String
)