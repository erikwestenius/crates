package se.westenius.discogs.shared.persistence.entity

import androidx.room.Entity

@Entity(
    primaryKeys = [
        "artistId",
        "artistReleaseId"
    ]
)
data class ArtistEntity(
    val artistId: Long,
    val artistReleaseId: Long,
    val artistName: String,
    val artistUrl: String
)

