package se.westenius.discogs.shared.data

data class UpdateState(
    val updated: Int,
    val total: Int
) {
    val progress: Float
        get() = if (total == 0) {
            0F
        } else {
            updated.toFloat() / total
        }


    companion object {
        val Unknown = UpdateState(-1, -1)
    }
}