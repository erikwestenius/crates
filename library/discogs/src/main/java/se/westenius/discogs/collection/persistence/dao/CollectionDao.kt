package se.westenius.discogs.collection.persistence.dao

import androidx.room.*
import kotlinx.coroutines.flow.Flow
import se.westenius.discogs.collection.persistence.entity.CollectedRelease
import se.westenius.discogs.collection.persistence.entity.CollectedReleaseEntity

@Dao
interface CollectionDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entities: List<CollectedReleaseEntity>)

    @Transaction
    @Query("SELECT * FROM CollectedReleaseEntity")
    fun collectedReleases(): Flow<List<CollectedRelease>>
}