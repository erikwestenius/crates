package se.westenius.discogs.wantlist.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Query
import se.westenius.discogs.network.BaseApiService
import se.westenius.discogs.network.Result
import se.westenius.discogs.shared.network.data.ApiBasicRelease
import se.westenius.discogs.shared.network.data.ApiPagination
import se.westenius.discogs.shared.network.data.PaginatedResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class WantlistApiService @Inject constructor(
    retrofit: Retrofit
) : BaseApiService<WantlistApiService.Api>(
    retrofit,
    Api::class.java
) {

    suspend fun getWantlist(page: Int = 0): Result<GetWantlistResponse> = apiCall {
        api.getWantlist(page)
    }

    interface Api {
        @GET("/users/{username}/wants")
        suspend fun getWantlist(
            @Query("page") page: Int = 0,
            @Query("per_page") perPage: Int = 100 //100 is max
        ): GetWantlistResponse
    }
}

@JsonClass(generateAdapter = true)
data class GetWantlistResponse(
    override val pagination: ApiPagination,
    val wants: List<ApiWantlistItem>
) : PaginatedResponse

@JsonClass(generateAdapter = true)
data class ApiWantlistItem(
    val rating: Int,
    @Json(name = "basic_information") val basicInfo: ApiBasicRelease
)