package se.westenius.discogs.search.data

data class SearchResult(
    val title: String
)