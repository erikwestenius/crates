package se.westenius.discogs.shared.persistence.entity

import androidx.room.Entity

@Entity(
    primaryKeys = [
        "formatReleaseId",
        "formatName"
    ]
)
data class FormatEntity(
    val formatReleaseId: Long,
    val formatQty: String,
    val formatName: String
)
