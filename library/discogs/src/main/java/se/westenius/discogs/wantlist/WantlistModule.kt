package se.westenius.discogs.wantlist

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import se.westenius.discogs.wantlist.persistence.WantlistDatabase
import se.westenius.discogs.wantlist.persistence.WantlistDatabaseImpl
import se.westenius.discogs.wantlist.repository.WantlistRepository
import se.westenius.discogs.wantlist.repository.WantlistRepositoryImpl

@Module
@InstallIn(ApplicationComponent::class)
abstract class WantlistModule {
    @Binds
    abstract fun bindWantlistRepository(
        impl: WantlistRepositoryImpl
    ): WantlistRepository

    @Binds
    abstract fun bindWantlistDatabase(
        impl: WantlistDatabaseImpl
    ): WantlistDatabase
}