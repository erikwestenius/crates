package se.westenius.discogs.collection.repository

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import se.westenius.discogs.BaseRepository
import se.westenius.discogs.collection.network.ApiReleaseVideo
import se.westenius.discogs.collection.network.ApiTrack
import se.westenius.discogs.collection.network.CollectionApiService
import se.westenius.discogs.collection.network.GetReleaseInfo
import se.westenius.discogs.collection.persistence.CollectionDatabase
import se.westenius.discogs.collection.repository.data.*
import se.westenius.discogs.network.Result
import se.westenius.discogs.shared.data.UpdateState
import se.westenius.discogs.shared.persistence.SharedDatabase
import javax.inject.Inject
import javax.inject.Singleton

interface CollectionRepository {
    val collectionValue: Flow<Double>
    val releases: Flow<List<Release>>
    val collectionUpdateState: Flow<UpdateState>
    fun releaseInfo(releaseId: Long): Flow<ReleaseInfo>

    suspend fun update()
}

@Singleton
class CollectionRepositoryImpl @Inject constructor(
    private val apiService: CollectionApiService,
    private val database: CollectionDatabase,
    sharedDatabase: SharedDatabase
) : BaseRepository(), CollectionRepository {

    override val releases: Flow<List<Release>> = database.collectionItems
    override val collectionValue: Flow<Double> = database.collectionItems
        .map { items ->
            items.sumByDouble { it.value.toDouble() }
        }

    override suspend fun update() = withPagination(
        request = { page ->
            apiService.getCollection(page)
        },
        response = {
            database.insert(it)
        }
    )

    override fun releaseInfo(releaseId: Long): Flow<ReleaseInfo> =
        flow {
            when (val result = apiService.getReleaseInfo(releaseId)) {
                is Result.Success -> emit(result.value.asReleaseInfo())
                is Result.Error -> throw Error(result.message)
            }
        }

    override val collectionUpdateState: Flow<UpdateState> = sharedDatabase.updateState

}

private fun GetReleaseInfo.asReleaseInfo() = ReleaseInfo(
    id = id,
    title = title,
    have = community.have,
    want = community.want,
    genres = (genres ?: emptyList()).plus(styles ?: emptyList()),
    thumbnailUrl = thumb,
    value = value,
    notes = notes,
    year = year ?: 0,
    numForSale = numForSale ?: 0,
    trackList = trackList?.map { it.asTrack() } ?: emptyList(),
    videos = videos?.map { it.asTrackVideo() } ?: emptyList(),
    labels = labels?.map { Label(it.id, it.name) } ?: emptyList(),
    artists = artists?.map { Artist(it.id, it.name) } ?: emptyList()
)

private fun ApiTrack.asTrack() = Track(
    title = title,
    duration = duration,
    position = position
)

private fun ApiReleaseVideo.asTrackVideo() = TrackVideo(
    title = title,
    duration = duration,
    description = description,
    url = uri
)
