package se.westenius.discogs.auth

import kotlinx.coroutines.flow.Flow

interface AuthPreferences {
    val authToken: Flow<String>
    suspend fun updateAuthToken(token: String)

    val authSecret: Flow<String>
    suspend fun updateAuthSecret(secret: String)

    val userName: Flow<String>
    suspend fun updateUserName(userName: String)
}