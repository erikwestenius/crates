package se.westenius.discogs.shared

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import se.westenius.discogs.shared.persistence.SharedDatabase
import se.westenius.discogs.shared.persistence.SharedDatabaseImpl

@Module
@InstallIn(ApplicationComponent::class)
internal abstract class SharedModule {
    @Binds
    abstract fun bindSharedDatabase(
        impl: SharedDatabaseImpl
    ) : SharedDatabase
}