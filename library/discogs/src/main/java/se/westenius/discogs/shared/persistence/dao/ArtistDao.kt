package se.westenius.discogs.shared.persistence.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import se.westenius.discogs.shared.persistence.entity.ArtistEntity

@Dao
interface ArtistDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entity: ArtistEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entities: List<ArtistEntity>)

    @Query("SELECT * FROM ArtistEntity")
    fun getAll(): Flow<List<ArtistEntity>>
}