package se.westenius.discogs

import kotlinx.coroutines.delay
import se.westenius.discogs.network.Result
import se.westenius.discogs.shared.network.data.PaginatedResponse

open class BaseRepository {
    suspend fun <T : PaginatedResponse> withPagination(
        request: suspend (page: Int) -> Result<T>,
        response: (T) -> Unit
    ) {
        var page = 0
        while (true) {
            val result = request(page)
            if (result !is Result.Success) {
                return
            }
            response(result.value)
            with(result.value.pagination) {
                if (page == pages) {
                    return
                } else {
                    page += 1
                }
            }
            //rate limiting is 60 requests per minute, sleep 1,5 secs after each request to mitigates
            delay(1500)
        }
    }
}