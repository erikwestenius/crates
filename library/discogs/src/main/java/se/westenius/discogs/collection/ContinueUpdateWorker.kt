package se.westenius.discogs.collection

import android.content.Context
import androidx.hilt.Assisted
import androidx.hilt.work.WorkerInject
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import se.westenius.discogs.collection.network.CollectionApiService
import se.westenius.discogs.collection.repository.CollectionRepository
import se.westenius.discogs.network.Result.Error
import se.westenius.discogs.network.Result.Success
import se.westenius.discogs.shared.persistence.SharedDatabase
import se.westenius.discogs.wantlist.repository.WantlistRepository
import timber.log.Timber
import javax.inject.Singleton

@Singleton
class ContinueUpdateWorker @WorkerInject constructor(
    @Assisted context: Context,
    @Assisted params: WorkerParameters,
    private val collectionRepository: CollectionRepository,
    private val wantlistRepository: WantlistRepository,
    private val database: SharedDatabase,
    private val apiService: CollectionApiService
) : CoroutineWorker(context, params) {

    override suspend fun doWork(): Result = withContext(Dispatchers.IO) {
        try {
            //only update collection if there are no dirty releases
            database.getCurrentUpdateState().apply {
                if (!isEmpty() && none { (_, dirty) -> dirty }) {
                    return@withContext Result.success()
                }
            }

            collectionRepository.update()
            wantlistRepository.update()

            database.getCurrentUpdateState()
                .apply {
                    if (isEmpty()) {
                        return@withContext Result.success()
                    }
                }
                .filter { (_, dirty) -> dirty }.let {
                    if (it.isEmpty()) {
                        database.markAllReleasesAsDirty()
                        database.getCurrentUpdateState()
                    } else {
                        it
                    }
                }.forEach { (id, _) ->
                    updateRelease(id)
                    delay(2000) //don't want to hit rate limit of 60 requests per minute
                }
            Result.success()
        } catch (e: Exception) {
            //todo handle RateLimitException
            Result.failure()
        }
    }

    private suspend fun updateRelease(id: Long) {
        when (val result = apiService.getReleaseInfo(id)) {
            is Success -> database.insert(result.value)
            is Error -> Timber.e("Could not fetch release with id: $id, message: ${result.message}")
        }
    }

    companion object {
        const val TAG = "ContinueUpdateWorker"
    }
}