package se.westenius.discogs.shared.persistence

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import se.westenius.discogs.collection.network.GetReleaseInfo
import se.westenius.discogs.collection.persistence.asArtist
import se.westenius.discogs.collection.persistence.asGenre
import se.westenius.discogs.collection.persistence.asLabel
import se.westenius.discogs.collection.repository.data.Release
import se.westenius.discogs.shared.data.UpdateState
import se.westenius.discogs.shared.network.data.ApiBasicArtist
import se.westenius.discogs.shared.network.data.ApiBasicFormat
import se.westenius.discogs.shared.network.data.ApiBasicLabel
import se.westenius.discogs.shared.network.data.ApiBasicRelease
import se.westenius.discogs.shared.persistence.dao.*
import se.westenius.discogs.shared.persistence.entity.*
import javax.inject.Inject
import javax.inject.Singleton

interface SharedDatabase {
    fun insert(releases: List<ApiBasicRelease>)

    fun getCurrentUpdateState(): List<Pair<Long, Boolean>>
    val updateState: Flow<UpdateState>
    fun insert(response: GetReleaseInfo)

    fun markAllReleasesAsDirty()

    fun recordWithId(id: Long): Flow<Release>
}

@Singleton
internal class SharedDatabaseImpl @Inject constructor(
    private val artistDao: ArtistDao,
    private val labelDao: LabelDao,
    private val formatDao: FormatDao,
    private val releaseDao: ReleaseDao,
    private val genreDao: GenreDao,
    private val releaseValueDao: ReleaseValueDao
) : SharedDatabase {

    override val updateState = releaseDao.getUpdateState()
        .map { flags ->
            UpdateState(
                flags.filter { !it.dirty }.count(),
                flags.count()
            )
        }

    override fun getCurrentUpdateState(): List<Pair<Long, Boolean>> =
        releaseDao.getCurrentUpdateState().map { Pair(it.releaseId, it.dirty) }

    override fun insert(response: GetReleaseInfo) {
        releaseValueDao.insert(response.asValueEntity())
        genreDao.insert(response.asGenreEntities())
        releaseDao.updateDirty(response.id, false)
    }

    override fun markAllReleasesAsDirty() {
        releaseDao.updateAllDirty(true)
    }

    override fun recordWithId(id: Long) = releaseDao.get(id)
        .map { it.asRelease() }

    override fun insert(releases: List<ApiBasicRelease>) {
        insertArtists(releases.flatMap { release ->
            release.artists.map { Pair(release.id, it) }
        })
        insertLabels(releases.flatMap { release ->
            release.labels.map {
                Pair(release.id, it)
            }
        })
        insertFormats(releases.flatMap { release ->
            release.formats.map {
                Pair(release.id, it)
            }
        })
        insertReleases(releases)
    }

    private fun insertArtists(artists: List<Pair<Long, ApiBasicArtist>>) =
        artistDao.insert(
            artists.map { (releaseId, artist) ->
                artist.asEntity(releaseId)
            }
        )

    private fun insertLabels(labels: List<Pair<Long, ApiBasicLabel>>) =
        labelDao.insert(
            labels.map { (releaseId, label) ->
                label.asEntity(releaseId)
            }
        )

    private fun insertFormats(formats: List<Pair<Long, ApiBasicFormat>>) =
        formatDao.insert(
            formats.map { (releaseId, format) ->
                format.asEntity(releaseId)
            }
        )

    private fun insertReleases(releases: List<ApiBasicRelease>) =
        releaseDao.insert(releases.map {
            it.asEntity()
        })
}

private fun GetReleaseInfo.asValueEntity() =
    ReleaseValueEntity(
        valueReleaseId = id,
        value = value ?: 0F,
        numForSale = numForSale ?: 0,
        have = community.have,
        want = community.want
    )

private fun GetReleaseInfo.asGenreEntities(): List<GenreEntity> =
    (genres?.map {
        GenreEntity(
            genreName = it,
            genreReleaseId = id
        )
    } ?: emptyList()).plus(styles?.map {
        GenreEntity(
            genreName = it,
            genreReleaseId = id
        )
    } ?: emptyList())

private fun ApiBasicRelease.asEntity() = ReleaseEntity(
    releaseId = id,
    releaseTitle = title.trim(),
    releaseYear = year,
    releaseThumbnailUrl = thumbnailUrl,
    releaseCoverImageUrl = coverImageUrl
)

private fun ApiBasicLabel.asEntity(releaseId: Long) = LabelEntity(
    labelId = id,
    labelReleaseId = releaseId,
    labelCatno = catno,
    labelName = name.trim(),
    labelEntityType = entityType,
    labelResourceUrl = resourceUrl
)

private fun ApiBasicArtist.asEntity(releaseId: Long) = ArtistEntity(
    artistId = id,
    artistReleaseId = releaseId,
    artistName = name.trim(),
    artistUrl = resourceUrl
)

private fun ApiBasicFormat.asEntity(releaseId: Long) = FormatEntity(
    formatReleaseId = releaseId,
    formatQty = qty,
    formatName = name
)

private fun PopulatedRelease.asRelease() =
    Release(
        id = release.releaseId,
        thumbnailUrl = release.releaseThumbnailUrl,
        title = release.releaseTitle,
        artists = releaseArtists.map { it.asArtist() },
        labels = releaseLabels.map { it.asLabel() },
        genres = genres.map { it.asGenre() },
        value = if (releaseValue.isNotEmpty()) {
            releaseValue[0].value
        } else 0F
    )