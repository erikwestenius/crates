package se.westenius.discogs.auth

import android.net.Uri
import android.net.UrlQuerySanitizer
import android.os.Parcelable
import android.os.SystemClock
import kotlinx.android.parcel.Parcelize
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.HeaderMap
import retrofit2.http.POST
import java.time.Instant
import javax.inject.Inject

private const val CONSUMER_KEY = "cDnGUMlMvVphFTZnZzNN"
private const val CONSUMER_SECRET = "hCyEzgNXArEmTaRYdsJkNCdEdFYPrtSu"
private const val CALLBACK_URL = "crates://auth_callback"

class AuthApiService @Inject constructor() :
    se.westenius.discogs.network.BaseApiService<AuthApiService.Api>(
        Retrofit.Builder()
            .baseUrl("https://api.discogs.com/")
            .addConverterFactory(ScalarsConverterFactory.create())
            .build(),
        Api::class.java
    ) {

    suspend fun requestToken() =
        apiCall {
            val token = api.requestToken(requestTokenHeaders)
            with(UrlQuerySanitizer("?${token.body()}")) {
                AuthTokenResponse(
                    oauthToken = getValue("oauth_token"),
                    oauthTokenSecret = getValue("oauth_token_secret"),
                    callbackConfirmed = getValue("oauth_callback_confirmed") == "true"
                )
            }
        }

    suspend fun getAccessToken(
        token: VerifiedToken,
        secret: String
    ) =
        apiCall {
            val accessToken = api.getAccessToken(
                token.asHeaders(secret)
            )
            with(UrlQuerySanitizer("?${accessToken.body()}")) {
                AccessToken(
                    oauthToken = getValue("oauth_token"),
                    oauthTokenSecret = getValue("oauth_token_secret"),
                )
            }
        }

    private val requestTokenHeaders = HashMap<String, String>().apply {
        this["User-Agent"] = "crates"
        this["Content-Type"] = "application/x-www-form-urlencoded"
        this["Authorization"] = "OAuth oauth_consumer_key=\"$CONSUMER_KEY\"," +
                " oauth_nonce=\"${SystemClock.uptimeMillis()}\"," +
                " oauth_signature=\"$CONSUMER_SECRET&\"," +
                " oauth_signature_method=\"PLAINTEXT\"," +
                " oauth_timestamp=\"${Instant.now().toEpochMilli()}\"," +
                " oauth_callback=\"${Uri.encode(CALLBACK_URL)}\""
                    .trimIndent()
    }

    private fun VerifiedToken.asHeaders(secret: String) = HashMap<String, String>().apply {
        this["User-Agent"] = "crates"
        this["Content-Type"] = "application/x-www-form-urlencoded"
        this["Authorization"] = "OAuth oauth_consumer_key=\"$CONSUMER_KEY\"," +
                " oauth_nonce=\"${SystemClock.uptimeMillis()}\"," +
                " oauth_token=\"$oauthToken\"," +
                " oauth_signature=\"$CONSUMER_SECRET&$secret\"," +
                " oauth_signature_method=\"PLAINTEXT\"," +
                " oauth_timestamp=\"${Instant.now().toEpochMilli()}\"," +
                " oauth_verifier=\"$oauthVerifier\""
                    .trimIndent()
    }

    interface Api {
        @GET("oauth/request_token")
        suspend fun requestToken(
            @HeaderMap headers: Map<String, String>
        ): Response<String>

        @POST("oauth/access_token")
        suspend fun getAccessToken(
            @HeaderMap headers: Map<String, String>
        ): Response<String>
    }
}

@Parcelize
data class AuthTokenResponse(
    val oauthToken: String,
    val oauthTokenSecret: String,
    val callbackConfirmed: Boolean
) : Parcelable

@Parcelize
data class VerifiedToken(
    val oauthToken: String,
    val oauthVerifier: String
) : Parcelable

@Parcelize
data class AccessToken(
    val oauthToken: String,
    val oauthTokenSecret: String
) : Parcelable