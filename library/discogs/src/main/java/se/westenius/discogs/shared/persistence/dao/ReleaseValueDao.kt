package se.westenius.discogs.shared.persistence.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import se.westenius.discogs.shared.persistence.entity.ReleaseValueEntity

@Dao
interface ReleaseValueDao {
    @Query("SELECT * FROM ReleaseValueEntity")
    fun getAll(): Flow<List<ReleaseValueEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entity: ReleaseValueEntity)
}