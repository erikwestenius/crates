package se.westenius.discogs.shared.persistence.entity

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation

@Entity
data class ReleaseEntity(
    @PrimaryKey val releaseId: Long,
    val releaseTitle: String,
    val releaseYear: Int,
    val releaseThumbnailUrl: String,
    val releaseCoverImageUrl: String,
    val dirty: Boolean = true
)

data class PopulatedRelease(
    @Embedded val release: ReleaseEntity,
    @Relation(
        parentColumn = "releaseId",
        entityColumn = "labelReleaseId"
    )
    val releaseLabels: List<LabelEntity>,
    @Relation(
        parentColumn = "releaseId",
        entityColumn = "artistReleaseId"
    )
    val releaseArtists: List<ArtistEntity>,
    @Relation(
        parentColumn = "releaseId",
        entityColumn = "formatReleaseId"
    )
    val releaseFormats: List<FormatEntity>,
    @Relation(
        parentColumn = "releaseId",
        entityColumn = "valueReleaseId",
    )
    val releaseValue: List<ReleaseValueEntity>,
    @Relation(
        parentColumn = "releaseId",
        entityColumn = "genreReleaseId"
    )
    val genres: List<GenreEntity>
)

data class ReleaseUpdateFlag(
    val releaseId: Long,
    val dirty: Boolean
)