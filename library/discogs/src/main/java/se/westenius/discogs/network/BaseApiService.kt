package se.westenius.discogs.network

import com.squareup.moshi.JsonClass
import retrofit2.HttpException
import retrofit2.Retrofit
import java.io.IOException

open class BaseApiService<Api : Any>(
    retrofit: Retrofit,
    private val apiClass: Class<Api>
) {

    protected val api: Api by lazy {
        retrofit.create(apiClass)
    }

    suspend fun <T> apiCall(apiCall: suspend () -> T): Result<T> =
        try {
            Result.Success(apiCall.invoke())
        } catch (throwable: Throwable) {
            when (throwable) {
                is IOException -> Result.Error.NetworkError(throwable.message)
                is HttpException -> Result.Error.HttpError(throwable.code(), throwable.message())
                else -> Result.Error.GenericError(throwable.message)
            }
        }
}

sealed class Result<out T> {
    data class Success<out T>(val value: T) : Result<T>()
    sealed class Error : Result<Nothing>() {
        abstract val message: String?

        data class GenericError(override val message: String? = null) : Error()
        data class NetworkError(override val message: String? = null) : Error()
        data class HttpError(val code: Int? = null, override val message: String? = null) : Error()
    }
}

@JsonClass(generateAdapter = true)
data class ErrorResponse(
    val errors: List<String>
)