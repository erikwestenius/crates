package se.westenius.discogs.collection.persistence.entity

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation
import se.westenius.discogs.shared.persistence.entity.*

@Entity
data class CollectedReleaseEntity(
    @PrimaryKey val releaseId: Long
)

data class CollectedRelease(
    @Embedded val collectedRelease: CollectedReleaseEntity,
    @Relation(
        parentColumn = "releaseId",
        entityColumn = "releaseId"
    )
    val release: ReleaseEntity,
    @Relation(
        parentColumn = "releaseId",
        entityColumn = "labelReleaseId"
    )
    val releaseLabels: List<LabelEntity>,
    @Relation(
        parentColumn = "releaseId",
        entityColumn = "artistReleaseId"
    )
    val releaseArtists: List<ArtistEntity>,
    @Relation(
        parentColumn = "releaseId",
        entityColumn = "formatReleaseId"
    )
    val releaseFormats: List<FormatEntity>,
    @Relation(
        parentColumn = "releaseId",
        entityColumn = "valueReleaseId",
    )
    val releaseValue: List<ReleaseValueEntity>,
    @Relation(
        parentColumn = "releaseId",
        entityColumn = "genreReleaseId"
    )
    val genres: List<GenreEntity>
)