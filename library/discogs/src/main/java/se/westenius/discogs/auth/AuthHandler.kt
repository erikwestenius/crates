package se.westenius.discogs.auth

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import se.westenius.discogs.network.Result
import se.westenius.discogs.user.User
import se.westenius.discogs.user.UserApiService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthHandler @Inject constructor(
    private val preferences: AuthPreferences,
    private val apiService: AuthApiService,
    private val userApiService: UserApiService
) {
    val isAuthorized: Flow<Boolean> =
        combine(
            preferences.authToken,
            preferences.authSecret,
            preferences.userName
        ) { token, secret, userName ->
            token.isNotEmpty() && secret.isNotEmpty() && userName.isNotEmpty()
        }

    private var unVerifiedSecret: String? = null

    suspend fun authorize(): Result<AuthTokenResponse> {
        return apiService.requestToken()
    }

    suspend fun getAccessToken(token: VerifiedToken): Result<AccessToken> {
        return unVerifiedSecret?.let {
            apiService.getAccessToken(token, it)
        } ?: Result.Error.GenericError("OathTokenSecret not set")
    }

    suspend fun persistAccessToken(accessToken: AccessToken) {
        preferences.updateAuthToken(accessToken.oauthToken)
        preferences.updateAuthSecret(accessToken.oauthTokenSecret)
    }

    suspend fun getUserIdentity(): Result<User> {
        return userApiService.getIdentity()
    }

    suspend fun persistUserName(userName: String) {
        preferences.updateUserName(userName)
    }

    fun cacheUnVerifiedSecret(oauthTokenSecret: String) {
        unVerifiedSecret = oauthTokenSecret
    }
}
