package se.westenius.discogs.wantlist.persistence

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import se.westenius.discogs.collection.persistence.asArtist
import se.westenius.discogs.collection.persistence.asGenre
import se.westenius.discogs.collection.persistence.asLabel
import se.westenius.discogs.collection.repository.data.Release
import se.westenius.discogs.shared.persistence.SharedDatabase
import se.westenius.discogs.wantlist.network.GetWantlistResponse
import se.westenius.discogs.wantlist.persistence.dao.WantlistDao
import se.westenius.discogs.wantlist.persistence.entity.WantlistRelease
import se.westenius.discogs.wantlist.persistence.entity.WantlistReleaseEntity
import javax.inject.Inject
import javax.inject.Singleton

interface WantlistDatabase {
    val wantlist: Flow<List<Release>>
    fun insert(response: GetWantlistResponse)
}

@Singleton
class WantlistDatabaseImpl @Inject constructor(
    private val sharedDatabase: SharedDatabase,
    private val wantlistDao: WantlistDao
) : WantlistDatabase {

    //todo handle removed wantlist items, right now they will be kept in the db..
    override val wantlist: Flow<List<Release>> = wantlistDao.getAll()
        .map { entities ->
            entities.map { it.asRelease() }
        }

    override fun insert(response: GetWantlistResponse) {
        sharedDatabase.insert(releases = response.wants.map { it.basicInfo })
        insertWantlist(response)
    }

    private fun insertWantlist(response: GetWantlistResponse) {
        wantlistDao.insertAll(
            response.wants.map { WantlistReleaseEntity(it.basicInfo.id) }
        )
    }
}

private fun WantlistRelease.asRelease() = Release(
    id = release.releaseId,
    thumbnailUrl = release.releaseThumbnailUrl,
    title = release.releaseTitle,
    artists = releaseArtists.map { it.asArtist() },
    labels = releaseLabels.map { it.asLabel() },
    genres = genres.map { it.asGenre() },
    value = if (releaseValue.isNotEmpty()) {
        releaseValue[0].value
    } else 0F
)