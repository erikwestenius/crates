package se.westenius.discogs.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import se.westenius.discogs.collection.persistence.dao.CollectionDao
import se.westenius.discogs.collection.persistence.entity.CollectedReleaseEntity
import se.westenius.discogs.persistence.converters.LocalDateConverter
import se.westenius.discogs.shared.persistence.dao.*
import se.westenius.discogs.shared.persistence.entity.*
import se.westenius.discogs.wantlist.persistence.dao.WantlistDao
import se.westenius.discogs.wantlist.persistence.entity.WantlistReleaseEntity

@Database(
    entities = [
        CollectedReleaseEntity::class,
        WantlistReleaseEntity::class,
        ReleaseValueEntity::class,
        ReleaseEntity::class,
        ArtistEntity::class,
        LabelEntity::class,
        FormatEntity::class,
        GenreEntity::class
    ], version = 1
)

@TypeConverters(LocalDateConverter::class)
abstract class DiscogsDatabase : RoomDatabase() {
    abstract fun collectionDao(): CollectionDao
    abstract fun wantlistDao(): WantlistDao
    abstract fun releaseDao(): ReleaseDao
    abstract fun artistDao(): ArtistDao
    abstract fun labelDao(): LabelDao
    abstract fun formatDao(): FormatDao
    abstract fun releaseValueDao(): ReleaseValueDao
    abstract fun genreDao(): GenreDao
}