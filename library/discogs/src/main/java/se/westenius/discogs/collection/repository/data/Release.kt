package se.westenius.discogs.collection.repository.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Release(
    val id: Long,
    val thumbnailUrl: String,
    val title: String,
    val artists: List<Artist>,
    val labels: List<Label>,
    val genres: List<Genre>,
    val value: Float
) : Parcelable {

    fun matchesFilter(filter: String?): Boolean =
        if (filter.isNullOrBlank()) {
            true
        } else {
            title.contains(filter, ignoreCase = true) ||
                    labels.joinToString { it.name }
                        .contains(filter, ignoreCase = true) ||
                    artists.joinToString { it.name }
                        .contains(filter, ignoreCase = true)
        }
}

@Parcelize
data class ReleaseInfo(
    val id: Long,
    val title: String,
    val thumbnailUrl: String?,
    val have: Int,
    val want: Int,
    val genres: List<String>,
    val value: Float?,
    val numForSale: Int,
    val trackList: List<Track>,
    val videos: List<TrackVideo>,
    val notes: String?,
    val year: Int?,
    val labels: List<Label>,
    val artists: List<Artist>
): Parcelable

@Parcelize
data class Track(
    val title: String,
    val duration: String,
    val position: String
) : Parcelable

@Parcelize
data class TrackVideo(
    val title: String,
    val duration: Int,
    val description: String,
    val url: String
) : Parcelable