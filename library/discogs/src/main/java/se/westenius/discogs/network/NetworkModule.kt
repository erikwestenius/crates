package se.westenius.discogs.network

import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import se.westenius.discogs.network.interceptors.HeaderInterceptor
import se.westenius.discogs.network.interceptors.LoggingInterceptor
import se.westenius.discogs.network.interceptors.UserNameInterceptor

private const val BASE_URL = "https://api.discogs.com/"

@Module
@InstallIn(ApplicationComponent::class)
class NetworkModule {

    @Provides
    fun provideMoshi(): Moshi = Moshi.Builder().build()

    @Provides
    fun provideRetrofit(
        headerInterceptor: HeaderInterceptor,
        userNameInterceptor: UserNameInterceptor,
        loggingInterceptor: LoggingInterceptor,
        moshi: Moshi
    ): Retrofit =
        Retrofit.Builder()
            .client(
                OkHttpClient.Builder()
                    .addInterceptor(headerInterceptor)
                    .addInterceptor(userNameInterceptor)
                    .addInterceptor(loggingInterceptor)
                    .build()
            )
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
}