package se.westenius.discogs.network

import kotlinx.coroutines.flow.Flow

interface NetworkPreferences {
    val authToken: Flow<String>
    val authSecret: Flow<String>
    val userName: Flow<String>
}