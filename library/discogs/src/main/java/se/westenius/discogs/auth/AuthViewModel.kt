package se.westenius.discogs.auth

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import se.westenius.discogs.network.Result
import timber.log.Timber

class AuthViewModel @ViewModelInject constructor(
    private val authHandler: AuthHandler
) : ViewModel() {

    val isLoading: LiveData<Boolean>
        get() = _isLoading
    private var _isLoading = MutableLiveData(false)

    val isAuthorized: LiveData<Boolean> = authHandler.isAuthorized.asLiveData()

    fun authorize(onCompleteCallback: (AuthTokenResponse) -> Unit) {
        viewModelScope.launch(Dispatchers.IO) {
            _isLoading.postValue(true)
            when (val result = authHandler.authorize()) {
                is Result.Success -> {
                    authHandler.cacheUnVerifiedSecret(result.value.oauthTokenSecret)
                    onCompleteCallback(result.value)
                }
                is Result.Error -> onError("Could not get request token: ${result.message}")
            }
        }
    }

    fun verifyAuthToken(token: VerifiedToken) {
        _isLoading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {
            getAndPersistAccessToken(
                token = token,
                onSuccess = ::getAndPersistUserIdentity
            )
        }
    }

    private suspend fun getAndPersistAccessToken(
        token: VerifiedToken,
        onSuccess: suspend () -> Unit
    ) {
        when (val result = authHandler.getAccessToken(token)) {
            is Result.Success -> {
                authHandler.persistAccessToken(result.value)
                onSuccess()
            }
            is Result.Error -> onError("Could not fetch access token: ${result.message}")
        }
    }

    private suspend fun getAndPersistUserIdentity() {
        when (val result = authHandler.getUserIdentity()) {
            is Result.Success -> authHandler.persistUserName(result.value.username)
            is Result.Error -> onError("Could not fetch user identity: ${result.message}")
        }
    }

    private fun onError(message: String) {
        Timber.e(message)
        _isLoading.postValue(false)
    }

}