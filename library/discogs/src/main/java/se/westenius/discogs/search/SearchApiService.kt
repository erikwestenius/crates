package se.westenius.discogs.search

import com.squareup.moshi.JsonClass
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import se.westenius.discogs.network.BaseApiService
import se.westenius.discogs.network.Result
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SearchApiService @Inject constructor(
    retrofit: Retrofit
) : BaseApiService<SearchApiService.Api>(retrofit, Api::class.java) {

    suspend fun search(query: String, type: SearchType): Result<GetSearchResult> = apiCall {
        api.search(query, type.toQuery())
    }

    suspend fun getArtist(id: Long): Result<GetArtistResult> = apiCall {
        api.getArtist(id.toString())
    }

    suspend fun getLabel(id: Long): Result<GetLabelResult> = apiCall {
        api.getLabel(id.toString())
    }

    interface Api {
        @GET("/database/search")
        suspend fun search(
            @Query("q") query: String,
            @Query("type") type: String
        ) : GetSearchResult

        @GET("artists/{artist_id}")
        suspend fun getArtist(
            @Path("artist_id") artistId: String
        ) : GetArtistResult

        @GET("labels/{label_id}")
        suspend fun getLabel(
            @Path("label_id") labelId: String
        ) : GetLabelResult
    }

    private fun SearchType.toQuery(): String {
        return when (this) {
            SearchType.ARTIST -> "artist"
            SearchType.RELEASE -> "release"
            SearchType.LABEL -> "label"
        }
    }
}

@JsonClass(generateAdapter = true)
data class GetSearchResult(
    val results: List<GetSearchResultItem>
)

@JsonClass(generateAdapter = true)
data class GetSearchResultItem(
    val id: Long,
    val title: String,
    val catno: String?,
    val year: String?,
    val style: List<String>?,
    val track: String?,
    val label: List<String>?
)

@JsonClass(generateAdapter = true)
data class GetArtistResult(
    val id: Long,
    val namevariations: List<String>?,
    val profile: String?,
    val members: List<Member>?
) {
    @JsonClass(generateAdapter = true)
    data class Member(
        val id: Long,
        val active: Boolean,
        val name: String?
    )
}

@JsonClass(generateAdapter = true)
data class GetLabelResult(
    val id: Long,
    val profile: String?,
    val name: String?,
    val sublabels: List<SubLabel>?
) {
    @JsonClass(generateAdapter = true)
    data class SubLabel(
        val id: Long,
        val name: String?
    )
}